<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function index()
    {
        $populers = Post::where('type', 'PUBLISH')->orderby('reader', 'desc')->limit(5)->get();
        $posts = Post::where('type', 'PUBLISH')->orderby('id', 'desc')->paginate(6);

        // dd($posts);
        return view('pages.blog.index_blog', compact('populers', 'posts'));
    }

    public function read($id = null)
    {
        Post::where('id', $id)->increment('reader');
        $data = Post::where('id', $id)->where('type', 'PUBLISH')->with('category')->first();
        $cover = url('cover_post/' . $data->cover);
        $categories = Category::get();
        $comments = Comment::where('post_id', $id)->where('reply_to', 0)->get();
        $replies = Comment::where('post_id', $id)->where('reply_to', '!=', 0)->get();
        $archives = Post::select(DB::raw("CONCAT(MONTH(created_at), '-', YEAR(created_at)) as archive"))->groupBy(DB::raw("CONCAT(MONTH(created_at), '-', YEAR(created_at))"))->get();

        return view('pages.blog.read_blog', compact('data', 'cover', 'categories', 'comments', 'archives', 'replies'));
    }

    public function search($type, $id = null)
    {

        if ($type == 'category') {
            if ($id != null) {
                $populers = Post::where('type', 'PUBLISH')->where('category_id', $id)->orderby('reader', 'desc')->limit(5)->get();
                $posts = Post::where('type', 'PUBLISH')->where('category_id', $id)->orderby('id', 'desc')->paginate(6);
                $type = Category::where('id', $id)->first()['name'];
            } else {
                $populers = Post::where('type', 'PUBLISH')->orderby('reader', 'desc')->limit(5)->get();
                $posts = Post::where('type', 'PUBLISH')->orderby('id', 'desc')->paginate(6);
            }
        } else if ($type == 'archive') {
            if ($id != null) {
                $date = explode('-', $id);
                $populers = Post::where('type', 'PUBLISH')
                    ->where(DB::raw('MONTH(created_at)'), $date[0])
                    ->where(DB::raw('YEAR(created_at)'), $date[1])
                    ->orderby('reader', 'desc')
                    ->limit(5)->get();
                $posts = Post::where('type', 'PUBLISH')
                    ->where(DB::raw('MONTH(created_at)'), $date[0])
                    ->where(DB::raw('YEAR(created_at)'), $date[1])
                    ->orderby('id', 'desc')->paginate(6);
                $type = $id;
            } else {
                $populers = Post::where('type', 'PUBLISH')->orderby('reader', 'desc')->limit(5)->get();
                $posts = Post::where('type', 'PUBLISH')->orderby('id', 'desc')->paginate(6);
            }
        } else {
            $populers = Post::where('type', 'PUBLISH')->orderby('reader', 'desc')->limit(5)->get();
            $posts = Post::where('type', 'PUBLISH')->where('title', 'like', "%$type%")->orderby('id', 'desc')->paginate(6);
        }

        return view('pages.blog.search_blog', compact('populers', 'posts', 'type'));
    }
}
