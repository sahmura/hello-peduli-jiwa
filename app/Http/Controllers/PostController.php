<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use DataTables;

class PostController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Post::orderby('id', 'desc')->with('category')->get();

            return DataTables::of($data)
                ->addColumn('category', function ($data) {
                    return $data->category->name ?? 'Tidak berkategori';
                })
                ->addColumn('artikel', function ($data) {
                    return $data->title;
                })
                ->addColumn('action', function ($data) {
                    return "<div class='btn-group'>
                        <button class='btn btn-warning btn-sm editButton' data-id='$data->id' data-name='$data->name' data-desc='$data->description'>
                            <i class='mdi mdi-pencil-outline'></i>
                        </button>
                        <button class='btn btn-danger btn-sm deleteButton' data-id='$data->id'>
                            <i class='mdi mdi-trash-can-outline'></i>
                        </button>
                    </div>";
                })
                ->rawColumns(['artikel', 'action'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('pages.post.index_post');
    }

    public function new()
    {
        $categories = Category::orderby('name', 'asc')->get();
        return view('pages.post.new_post', compact('categories'));
    }

    public function edit($id)
    {
        $data = Post::where('id', $id)->first();
        $categories = Category::orderby('name', 'asc')->get();

        return view('pages.post.edit_post', compact('data', 'categories'));
    }


    public function create(Request $request)
    {

        if ($request->hasFile('cover')) {
            $imageName = time() . '.' . $request->cover->extension();
            $request->cover->move(public_path('cover_post'), $imageName);
        } else {
            $imageName = 'cover_default.svg';
        }


        $slang = str_replace(' ', '+', $request->title);
        $checkSlang = Post::where('slang', $slang)->count();

        if ($checkSlang != 0) {
            $slang = $slang . '+' . $checkSlang;
        }

        $act = Post::create([
            'category_id' => $request->category_id,
            'slang' => $slang,
            'title' => $request->title,
            'content' => $request->content,
            'cover' => $imageName,
            'type' => $request->type,
            'tags' => $request->tags,
            'reader' => 0,
        ]);

        if ($act) {
            return response()->json($this->sendNotification('add-success'));
        } else {
            return response()->json($this->sendNotification('add-error'));
        }
    }

    public function update(Request $request)
    {

        $oldData = Post::where('id', $request->id)->first();

        if ($request->cover != 'not') {
            if (\File::exists(public_path($oldData->cover))) {
                \File::delete(public_path($oldData->cover));
            }
            $imageName = time() . '.' . $request->cover->extension();
            $request->cover->move(public_path('cover_post'), $imageName);
        } else {
            $imageName = $oldData->cover;
        }

        $slang = str_replace(' ', '+', $request->title);
        $checkSlang = Post::where('slang', $slang)->whereNotIn('id', [$request->id])->count();

        if ($checkSlang != 0) {
            $slang = $slang . '+' . $checkSlang;
        }

        $act = Post::where('id', $request->id)->update([
            'category_id' => $request->category_id,
            'slang' => $slang,
            'title' => $request->title,
            'content' => $request->content,
            'cover' => $imageName,
            'type' => $request->type,
            'tags' => $request->tags,
        ]);

        if ($act) {
            return response()->json($this->sendNotification('edit-success'));
        } else {
            return response()->json($this->sendNotification('edit-error'));
        }
    }

    public function delete(Request $request)
    {
        $act = Post::where('id', $request->id)->delete();

        if ($act) {
            return response()->json($this->sendNotification('delete-success'));
        } else {
            return response()->json($this->sendNotification('delete-error'));
        }
    }
}
