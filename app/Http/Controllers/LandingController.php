<?php

namespace App\Http\Controllers;

use App\Models\Statistic;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    public function index()
    {
        $lastStats = Statistic::where('tanggal', date('Y-m-d'))->first();
        if ($lastStats != null) {
            Statistic::where('tanggal', date('Y-m-d'))->increment('stats', 1);
        } else {
            Statistic::create(
                [
                    'tanggal' => date('Y-m-d'),
                    'stats' => 0
                ]
            );
        }

        return view('welcome');
    }
}
