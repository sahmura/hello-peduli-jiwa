<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function get()
    {
        $data = Notification::where('to_id', Auth()->user()->id)->where('is_readed', 0)->orderby('id', 'desc')->get();
        return response()->json($data);
    }

    public function setRead(Request $request)
    {
        Notification::where('id', $request->id)->update(['is_readed' => 1]);
    }
}
