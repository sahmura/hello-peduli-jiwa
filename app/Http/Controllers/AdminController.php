<?php

namespace App\Http\Controllers;

use App\Models\Creation;
use App\Models\Post;
use App\Models\Statistic;
use App\Models\Topic;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    public function index()
    {
        $total['post'] = Post::get()->count();
        $total['sharing'] = Topic::get()->count();
        $total['karya'] = Creation::get()->count();
        $total['user'] = User::get()->count();

        $latestTopic = Topic::orderBy('id', 'desc')->with('user')->limit(10)->get();
        $stats = Statistic::select(DB::raw('SUM(stats) as stats, tanggal'))->groupby('tanggal')->orderby('tanggal', 'desc')->limit(10)->get();

        return view('pages.dashboard.admin_dashboard', compact('total', 'latestTopic', 'stats'));
    }

    public function user(Request $request)
    {

        if ($request->ajax()) {
            $data = User::where('role', '!=', 'admin')->get();

            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    return "<div class='btn-group'>
                        <button class='btn btn-primary btn-sm detailButton' data-id='$data->id'>
                            <i class='mdi mdi-eye-outline'></i>
                        </button>
                    </div>";
                })
                ->addIndexColumn()
                ->make(true);
        }

        return view('pages.dashboard.user_list_admin');
    }

    public function detailUser(Request $request)
    {
        $totalCreation = Creation::where('user_id', $request->id)->count();
        $totalTopic = Topic::where('user_id', $request->id)->count();
        $detail = User::where('id', $request->id)->first();

        $res = [
            'detail' => $detail,
            'totalTopic' => $totalTopic,
            'totalCreation' => $totalCreation,
        ];

        return response()->json($res);
    }
}
