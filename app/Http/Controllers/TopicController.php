<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Topic;
use App\Models\TopicDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class TopicController extends Controller
{
    public function admin()
    {
        return view('pages.sharing.index_admin_sharing');
    }

    public function list_admin(Request $request)
    {
        if ($request->ajax()) {
            $data = Topic::with('user')->orderBy('id', 'desc');

            if ($request->tgl_awal) {
                $data->where(DB::raw('LEFT(created_at, 10)'), '>=', $request->tgl_awal);
            }
            if ($request->tgl_akhir) {
                $data->where(DB::raw('LEFT(created_at, 10)'), '<=', $request->tgl_akhir);
            }

            $res = $data->get();

            return DataTables::of($res)
                ->addColumn('action', function ($res) {
                    return "<div class='btn-group'>
                        <button class='btn btn-primary btn-sm showButton' data-id='$res->id'>
                            <i class='mdi mdi-email-open-outline'></i>
                        </button>
                        <button class='btn btn-danger btn-sm deleteButton' data-id='$res->id'>
                            <i class='mdi mdi-trash-can-outline'></i>
                        </button>
                    </div>";
                })
                ->addIndexColumn()
                ->make(true);
        }
    }

    public function user(Request $request)
    {
        if ($request->ajax()) {
            $data = Topic::where('user_id', Auth()->user()->id)->get();
            return response()->json($data);
        }

        return view('pages.sharing.index_user_sharing');
    }

    public function addMessage(Request $request)
    {
        if (Auth()->user()->role == 'admin') {
            $cta = url('sharing/detail/' . $request->topic_id);
        } else {
            $cta = url('admin/sharing/detail/' . $request->topic_id);
        }

        $act = TopicDetail::create([
            'user_id' => Auth()->user()->id,
            'topic_id' => $request->topic_id,
            'message' => $request->message
        ]);

        $notif = Notification::create([
            'from_id' => Auth()->user()->id,
            'to_id' => $request->to_id,
            'title' => Auth()->user()->name . ' membalas topic',
            'cta' => $cta,
            'is_readed' => 0
        ]);

        if ($act) {
            return response()->json($this->sendNotification('add-success'));
        } else {
            return response()->json($this->sendNotification('add-error'));
        }
    }

    public function editMessage(Request $request)
    {
        $act = TopicDetail::where('id', $request->id)->update([
            'message' => $request->message
        ]);

        if ($act) {
            return response()->json($this->sendNotification('edit-success'));
        } else {
            return response()->json($this->sendNotification('edit-error'));
        }
    }

    public function deleteMessage(Request $request)
    {
        $act = TopicDetail::where('topic_id', $request->id)->delete();
        $topic = Topic::where('id', $request->id)->delete();

        if ($act && $topic) {
            return response()->json($this->sendNotification('delete-success'));
        } else {
            return response()->json($this->sendNotification('delete-error'));
        }
    }

    public function showTopic($id = null)
    {
        $data = TopicDetail::where('topic_id', $id)->get();
        $detail = Topic::where('id', $id)->with('user')->first();

        return view('pages.sharing.index_admin_show_sharing', compact('data', 'detail'));
    }

    public function createTopic(Request $request)
    {
        $act = Topic::create([
            'user_id' => Auth()->user()->id,
            'topic' => $request->topic,
            'description' => $request->description
        ]);

        if ($act) {
            return response()->json($this->sendNotification('add-success'));
        } else {
            return response()->json($this->sendNotification('add-error'));
        }
    }

    public function editTopic(Request $request)
    {
        $act = Topic::where('id', $request->id)->update([
            'topic' => $request->topic,
            'description' => $request->description
        ]);

        if ($act) {
            return response()->json($this->sendNotification('edit-success'));
        } else {
            return response()->json($this->sendNotification('edit-error'));
        }
    }

    public function deleteTopic(Request $request)
    {
        try {
            DB::beginTransaction();
            $act = Topic::where('id', $request->id)->delete();
            $act_detail = TopicDetail::where('topic_id', $request->id)->delete();
            DB::commit();
            if ($act) {
                return response()->json($this->sendNotification('delete-success'));
            } else {
                return response()->json($this->sendNotification('delete-error'));
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json($this->sendNotification('delete-error'));
        }
    }

    public function user_index()
    {
        return view('pages.sharing.index_user_sharing');
    }

    public function user_lists(Request $request)
    {
        if ($request->ajax()) {
            $data = Topic::where('user_id', Auth()->user()->id)->with('user')->orderBy('id', 'desc');

            if ($request->tgl_awal) {
                $data->where(DB::raw('LEFT(created_at, 10)'), '>=', $request->tgl_awal);
            }
            if ($request->tgl_akhir) {
                $data->where(DB::raw('LEFT(created_at, 10)'), '<=', $request->tgl_akhir);
            }

            $res = $data->get();

            return DataTables::of($res)
                ->addColumn('action', function ($res) {
                    return "<div class='btn-group'>
                        <button class='btn btn-primary btn-sm showButton' data-id='$res->id'>
                            <i class='mdi mdi-email-open-outline'></i>
                        </button>
                        <button class='btn btn-warning btn-sm' data-id='$res->id' data-topic='$res->topic' data-description='$res->description' onclick='neworupdate(this)'>
                            <i class='mdi mdi mdi-pencil-outline'></i>
                        </button>
                        <button class='btn btn-danger btn-sm deleteButton' data-id='$res->id'>
                            <i class='mdi mdi-trash-can-outline'></i>
                        </button>
                    </div>";
                })
                ->addIndexColumn()
                ->make(true);
        }
    }

    public function showTopicUser($id = null)
    {
        $data = TopicDetail::where('topic_id', $id)->get();
        $detail = Topic::where('id', $id)->where('user_id', Auth()->user()->id)->with('user')->first();

        return view('pages.sharing.index_user_show_sharing', compact('data', 'detail'));
    }
}
