<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;
use DataTables;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Category::get();

            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    return "<div class='btn-group'>
                        <button class='btn btn-warning btn-sm editButton' data-id='$data->id' data-name='$data->name' data-desc='$data->description'>
                            <i class='mdi mdi-pencil-outline'></i>
                        </button>
                        <button class='btn btn-danger btn-sm deleteButton' data-id='$data->id'>
                            <i class='mdi mdi-trash-can-outline'></i>
                        </button>
                    </div>";
                })
                ->addIndexColumn()
                ->make(true);
        }

        return view('pages.category.index_category');
    }

    public function create(Request $request)
    {
        if (Category::where('name', $request->name)->count() != 0) {
            return response()->json($this->sendNotification('error', 'Data sudah tersedia', 'Kategori ' . $request->name . ' sudah tersedia', 'error'));
        }
        $act = Category::create([
            'name' => $request->name,
            'description' => $request->description
        ]);

        if ($act) {
            return response()->json($this->sendNotification('add-success'));
        } else {
            return response()->json($this->sendNotification('add-error'));
        }
    }

    public function update(Request $request)
    {
        if (Category::where('name', $request->name)->whereNotIn('id', [$request->id])->count() != 0) {
            return response()->json($this->sendNotification('error', 'Data sudah tersedia', 'Kategori ' . $request->name . ' sudah tersedia', 'error'));
        }

        $act = Category::where('id', $request->id)->update([
            'name' => $request->name,
            'description' => $request->description
        ]);

        if ($act) {
            return response()->json($this->sendNotification('edit-success'));
        } else {
            return response()->json($this->sendNotification('edit-error'));
        }
    }

    public function delete(Request $request)
    {
        $act = Category::where('id', $request->id)->delete();
        $changeCategoryPost = Post::where('category_id', $request->id)->update([
            'category_id' => 0
        ]);
        if ($act && $changeCategoryPost) {
            return response()->json($this->sendNotification('delete-success'));
        } else {
            return response()->json($this->sendNotification('delete-error'));
        }
    }
}
