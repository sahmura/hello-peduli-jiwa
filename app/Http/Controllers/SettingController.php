<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class SettingController extends Controller
{
    public function index()
    {
        return view('pages.settings.index_setting');
    }

    public function updateProfil(Request $request)
    {
        $act = User::where('id', Auth()->user()->id)->update(
            [
                'name' => $request->name,
                'email' => $request->email
            ]
        );

        if ($act) {
            return response()->json($this->sendNotification('save-success'));
        } else {
            return response()->json($this->sendNotification('save-error'));
        }
    }

    public function updatePassword(Request $request)
    {
        $checkOldPassword = User::where('id', Auth()->user()->id)->first();
        
        if (Hash::check($request->oldPassword, $checkOldPassword->password)) {

            $act = User::where('id', Auth()->user()->id)->update([
                'password' => Hash::make($request->newPassword)
            ]);

            if ($act) {
                return response()->json($this->sendNotification('save-success'));
            } else {
                return response()->json($this->sendNotification('save-error'));
            }

        } else {
            return response()->json($this->sendNotification('error-password', 'Password lama tidak sesuai', '', 'error'));
        }
    }
}
