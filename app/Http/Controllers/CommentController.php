<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use DataTables;

class CommentController extends Controller
{
    public function addComment(Request $request)
    {
        try {
            DB::beginTransaction();
            $post = Post::where('id', $request->post_id)->first();
            $listAdmin = User::where('role', 'admin')->get();
            Comment::create(
                [
                    'user_id' => Auth()->user()->id,
                    'post_id' => $request->post_id,
                    'name' => $request->name,
                    'comment' => $request->comment
                ]
            );

            foreach ($listAdmin as $admin) {
                Notification::create(
                    [
                        'from_id' => Auth()->user()->id ?? 0,
                        'to_id' => $admin->id,
                        'title' => 'Komentar baru dari ' . $request->name,
                        'cta' => "<a href='" . url('read/' . $post->id . '/' . $post->slang) . "'>Komentar baru dari $request->name</a>"
                    ]
                );
            }
            DB::commit();

            return response()->json($this->sendNotification('comment', 'Berhasil menambahkan komentar', '', 'success'));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json($this->sendNotification('comment', 'Gagal menambahkan komentar', $e->getMessage(), 'error'));
        }
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Comment::where('reply_to', 0)->with('post')->get();

            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    return "<div class='btn-group'>
                        <button class='btn btn-primary btn-sm detailButton' data-id='$data->id'>
                            <i class='mdi mdi mdi-magnify'></i>
                        </button>
                        <button class='btn btn-danger btn-sm deleteButton' data-id='$data->id'>
                            <i class='mdi mdi-trash-can-outline'></i>
                        </button>
                    </div>";
                })
                ->addIndexColumn()
                ->make(true);
        }

        return view('pages.comment.index_comment');
    }

    public function detailComment(Request $request)
    {
        $data['comment'] = Comment::where('id', $request->id)->with('post')->first();
        $data['reply'] = Comment::where('reply_to', $request->id)->with('post')->orderby('id', 'asc')->get();

        return response()->json($data);
    }

    public function delete(Request $request)
    {
        $act = Comment::where('id', $request->id)->delete();
        $replyComment = Comment::where('reply_to', $request->id)->delete();

        if ($act && $replyComment) {
            return response()->json($this->sendNotification('delete-success'));
        } else {
            return response()->json($this->sendNotification('delete-error'));
        }
    }

    public function replyTo(Request $request)
    {
        $act = Comment::create(
            [
                'user_id' => Auth()->user()->id,
                'post_id' => $request->post_id,
                'name' => Auth()->user()->name,
                'comment' => $request->reply_comment,
                'reply_to' => $request->reply_to
            ]
        );

        if ($act) {
            return response()->json($this->sendNotification('add-success'));
        } else {
            return response()->json($this->sendNotification('add-error'));
        }
    }
}
