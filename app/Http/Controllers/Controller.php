<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use DataTables;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendNotification($type = null, $title = null, $text = null, $icon = null)
    {

        switch ($type) {
            case 'add-success':
                $res = [
                    'title' => 'Berhasil menambahkan data',
                    'text' => '',
                    'icon' => 'success'
                ];
                break;
            case 'edit-success':
                $res = [
                    'title' => 'Berhasil mengubah data',
                    'text' => '',
                    'icon' => 'success'
                ];
                break;
            case 'delete-success':
                $res = [
                    'title' => 'Berhasil menghapus data',
                    'text' => '',
                    'icon' => 'success'
                ];
                break;
            case 'save-success':
                $res = [
                    'title' => 'Berhasil menyimpan data',
                    'text' => '',
                    'icon' => 'success'
                ];
                break;
            case 'add-error':
                $res = [
                    'title' => 'Gagal menambahkan data',
                    'text' => '',
                    'icon' => 'error'
                ];
                break;
            case 'edit-error':
                $res = [
                    'title' => 'Gagal mengubah data',
                    'text' => '',
                    'icon' => 'error'
                ];
                break;
            case 'delete-error':
                $res = [
                    'title' => 'Gagal menghapus data',
                    'text' => '',
                    'icon' => 'error'
                ];
                break;
            case 'save-error':
                $res = [
                    'title' => 'Gagal menyimpan data',
                    'text' => '',
                    'icon' => 'error'
                ];
                break;
            default:
                $res = [
                    'title' => $title,
                    'text' => $text,
                    'icon' => $icon
                ];
                break;
        }

        return $res;
    }
}
