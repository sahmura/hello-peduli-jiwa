<?php

namespace App\Http\Controllers;

use App\Models\Creation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use DataTables;
use Str;

class CreationController extends Controller
{
    public function index()
    {
        return view('pages.creation.index_creation');
    }

    public function create(Request $request)
    {
        $creation = $request->file('creation');

        if (!in_array($creation->getClientOriginalExtension(), ['jpg', 'png', 'jpeg'])) {
            return response()->json($this->sendNotification('batas', 'Gagal menambahkan karya', 'File yang diperbolehkan sementara hanya gambar', 'error'));
        }

        $fileName = Str::slug(Auth()->user()->name, '-') . '-' . date('Y-m-d') . '.' . $creation->getClientOriginalExtension();
        $request->creation->move(public_path('creationfile'), $fileName);

        $act = Creation::create(
            [
                'user_id' => Auth()->user()->id,
                'name' => $request->name,
                'desc' => $request->desc,
                'extension' => $creation->getClientOriginalExtension(),
                'file' => $fileName
            ]
        );

        if ($act) {
            return response()->json($this->sendNotification('add-success'));
        } else {
            return response()->json($this->sendNotification('add-error'));
        }
    }

    public function update(Request $request)
    {
        $oldData = Creation::where('id', $request->id)->first();

        if ($request->hasFile('creation')) {
            $creation = $request->file('creation');

            if (!in_array($creation->getClientOriginalExtension(), ['jpg', 'png', 'jpeg'])) {
                return response()->json($this->sendNotification('batas', 'Gagal menambahkan karya', 'File yang diperbolehkan sementara hanya gambar', 'error'));
            }

            $fileName = Str::slug(Auth()->user()->name, '-') . '-' . date('Y-m-d') . '.' . $creation->getClientOriginalExtension();
            File::delete(public_path('creationfile/' . $oldData->file));
            $request->creation->move(public_path('creationfile'), $fileName);
            $ext = $creation->getClientOriginalExtension();
        } else {
            $fileName = $oldData->file;
            $ext = $oldData->extension;
        }

        $act = Creation::where('id', $request->id)->where('user_id', Auth()->user()->id)->update(
            [
                'name' => $request->name,
                'desc' => $request->desc,
                'extension' => $ext,
                'file' => $fileName
            ]
        );

        if ($act) {
            return response()->json($this->sendNotification('edit-success'));
        } else {
            return response()->json($this->sendNotification('edit-error'));
        }
    }

    public function delete(Request $request)
    {
        $oldData = Creation::where('id', $request->id)->where('user_id', Auth()->user()->id)->first();
        $act = Creation::where('id', $request->id)->delete();
        File::delete(public_path('creationfile/' . $oldData->file));

        if ($act) {
            return response()->json($this->sendNotification('delete-success'));
        } else {
            return response()->json($this->sendNotification('delete-error'));
        }
    }

    public function getList(Request $request)
    {
        $data = Creation::where('user_id', Auth()->user()->id)->orderby('id', 'desc')->get()->toArray();

        return response()->json($data);
    }

    public function indexAdmin(Request $request)
    {
        if ($request->ajax()) {
            $data = Creation::orderby('id', 'desc')->with('user')->get();

            return DataTables::of($data)
                ->addColumn('action', function ($data) {
                    return "<div class='btn-group'>
                        <button class='btn btn-primary btn-sm detailButton' data-id='$data->id'>
                            <i class='mdi mdi-eye-outline'></i>
                        </button>
                        <button class='btn btn-danger btn-sm deleteButton' data-id='$data->id'>
                            <i class='mdi mdi-trash-can-outline'></i>
                        </button>
                    </div>";
                })
                ->addIndexColumn()
                ->make(true);
        }

        return view('pages.creation.index_admin');
    }

    public function indexPublic()
    {
        $data = Creation::orderby('id', 'desc')->paginate(12);

        return view('pages.creation.index_public', compact('data'));
    }

    public function search(Request $request)
    {
        $data = Creation::where('name', 'like', "%$request->name%")->get()->toArray();

        return response()->json($data);
    }

    public function detailFromDashboard($id)
    {
        $data = Creation::where('user_id', Auth()->user()->id)->where('id', $id)->first();

        return view('pages.creation.detail_creation', compact('data'));
    }

    public function look($id)
    {
        $data = Creation::where('id', $id)->first();

        return view('pages.creation.look_public', compact('data'));
    }

    public function deleteAdmin(Request $request)
    {
        $act = Creation::where('id', $request->id)->delete();

        if ($act) {
            return response()->json($this->sendNotification('delete-success'));
        } else {
            return response()->json($this->sendNotification('delete-error'));
        }
    }

    public function getFromAdmin(Request $request)
    {
        return response()->json(Creation::where('id', $request->id)->with('user')->first());
    }
}
