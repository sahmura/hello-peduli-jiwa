<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\CreationController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LandingController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\TopicController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LandingController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::post('getNotification', [NotificationController::class, 'get']);
Route::post('setReadNotif', [NotificationController::class, 'setRead']);

Route::group(['prefix' => 'blog'], function () {
    Route::get('/', [BlogController::class, 'index']);
    Route::get('search/{type}/{id?}', [BlogController::class, 'search']);
});

Route::group(['prefix' => 'creations'], function () {
    Route::get('/', [CreationController::class, 'indexPublic']);
    Route::get('search/{type}/{id?}', [CreationController::class, 'search']);
});

Route::group(['prefix' => 'read'], function () {
    Route::get('/{id}/{slang}', [BlogController::class, 'read']);
});

Route::group(['prefix' => 'look'], function () {
    Route::get('/{id}', [CreationController::class, 'look']);
});

Route::post('/comment/add', [CommentController::class, 'addComment']);

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'checkrole']], function () {
    Route::get('/', [AdminController::class, 'index']);

    Route::group(['prefix' => 'category'], function () {
        Route::get('/', [CategoryController::class, 'index']);
        Route::post('/create', [CategoryController::class, 'create']);
        Route::post('/update', [CategoryController::class, 'update']);
        Route::delete('/delete', [CategoryController::class, 'delete']);
    });

    Route::group(['prefix' => 'post'], function () {
        Route::get('/', [PostController::class, 'index']);
        Route::get('/new', [PostController::class, 'new']);
        Route::get('/{id}/edit', [PostController::class, 'edit']);
        Route::post('/create', [PostController::class, 'create']);
        Route::post('/update', [PostController::class, 'update']);
        Route::delete('/delete', [PostController::class, 'delete']);
    });

    Route::group(['prefix' => 'sharing'], function () {
        Route::get('/', [TopicController::class, 'admin']);
        Route::post('/list_admin', [TopicController::class, 'list_admin']);
        Route::delete('/delete', [TopicController::class, 'deleteMessage']);
        Route::get('/detail/{id}', [TopicController::class, 'showTopic']);
        Route::post('/reply', [TopicController::class, 'addMessage']);
    });

    Route::group(['prefix' => 'comment'], function () {
        Route::get('/', [CommentController::class, 'index']);
        Route::post('/detail', [CommentController::class, 'detailComment']);
        Route::delete('/delete', [CommentController::class, 'delete']);
        Route::post('/reply', [CommentController::class, 'replyTo']);
    });

    Route::group(['prefix' => 'creation'], function () {
        Route::get('/', [CreationController::class, 'indexAdmin']);
        Route::delete('/delete', [CreationController::class, 'deleteAdmin']);
        Route::post('/get', [CreationController::class, 'getFromAdmin']);
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', [AdminController::class, 'user']);
        Route::post('/detail', [AdminController::class, 'detailUser']);
    });
});

Route::group(['middleware' => ['auth']], function () {

    Route::get('dashboard', [DashboardController::class, 'index']);

    Route::group(['prefix' => 'sharing'], function () {
        Route::get('/', [TopicController::class, 'user_index']);
        Route::post('/store', [TopicController::class, 'createTopic']);
        Route::put('/update', [TopicController::class, 'editTopic']);
        Route::delete('/delete', [TopicController::class, 'deleteTopic']);
        Route::post('/lists', [TopicController::class, 'user_lists']);
        Route::get('/detail/{id}', [TopicController::class, 'showTopicUser']);
    });

    Route::group(['prefix' => 'settings'], function () {
        Route::get('/', [SettingController::class, 'index']);
        Route::post('/updateProfil', [SettingController::class, 'updateProfil']);
        Route::post('/updatePassword', [SettingController::class, 'updatePassword']);
    });

    Route::group(['prefix' => 'sharing'], function () {
        Route::post('/addMessage', [TopicController::class, 'addMessage']);
        Route::post('/editMessage', [TopicController::class, 'editMessage']);
        Route::post('/deleteMessage', [TopicController::class, 'deleteMessage']);
        Route::post('/createTopic', [TopicController::class, 'createTopic']);
        Route::post('/editTopic', [TopicController::class, 'editTopic']);
        Route::post('/deleteTopic', [TopicController::class, 'deleteTopic']);
        Route::post('/reply', [TopicController::class, 'addMessage']);
    });

    Route::group(['prefix' => 'creation'], function () {
        Route::get('/', [CreationController::class, 'index']);
        Route::post('create', [CreationController::class, 'create']);
        Route::post('update', [CreationController::class, 'update']);
        Route::delete('delete', [CreationController::class, 'delete']);
        Route::post('lists', [CreationController::class, 'getList']);
        Route::get('detail/{id}', [CreationController::class, 'detailFromDashboard']);
    });
});
