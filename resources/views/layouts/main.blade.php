<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Hello Peduli Jiwa</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Hello Peduli Jiwa merupakan platform yang menyediakan berbagai konten tentang kesehatan mental" name="description" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="57x57" href="assetsicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assetsicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assetsicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assetsicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assetsicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assetsicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assetsicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assetsicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assetsicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assetsicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assetsicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assetsicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assetsicon/favicon-16x16.png">
    <link rel="manifest" href="assetsicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assetsicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#00807a">

    <link href="{{ url('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">\

    <link href="{{ url('assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />

    @stack('css')

</head>

<body data-topbar="colored">

    <div id="layout-wrapper">

        <header id="page-topbar">
            <div class="navbar-header">
                <div class="d-flex">
                    <div class="navbar-brand-box">
                        <a href="{{ url('') }}" class="logo logo-dark text-center">
                            <span class="logo-sm">
                                <img src="{{ url('logo-color.svg') }}" alt="" height="30">
                            </span>
                            <span class="logo-lg">
                                <img src="{{ url('full-logo-color-horizontal.svg') }}" alt="" height="50">
                            </span>
                        </a>

                        <a href="{{ url('') }}" class="logo logo-light text-center">
                            <span class="logo-sm">
                                <img src="{{ url('logo-color.svg') }}" alt="" height="30">
                            </span>
                            <span class="logo-lg">
                                <img src="{{ url('full-logo-color-horizontal.svg') }}" alt="" height="50">
                            </span>
                        </a>
                    </div>

                    <button type="button" class="btn btn-sm px-3 font-size-24 header-item waves-effect" id="vertical-menu-btn">
                        <i class="mdi mdi-backburger"></i>
                    </button>

                    <form class="app-search d-none d-lg-block">
                        <div class="position-relative">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="mdi mdi-magnify"></span>
                        </div>
                    </form>
                </div>

                <div class="d-flex">
                    <div class="dropdown d-inline-block">
                        <button type="button" class="btn header-item waves-effect" id="page-header-flag-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="mdi mdi-bell"></i> <span id="totalNotif"></span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" id="notif-area">
                        </div>
                    </div>

                    <div class="dropdown d-inline-block">
                        <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="rounded-circle header-profile-user" src="https://ui-avatars.com/api/?name={{ str_replace(' ', '+', Auth()->user()->name) }}" alt="{{ Auth()->user()->name }}">
                            <span class="d-none d-sm-inline-block ml-1">{{ Auth()->user()->name }}</span>
                            <i class="mdi mdi-chevron-down d-none d-sm-inline-block"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#"><i class="mdi mdi-face-profile font-size-16 align-middle mr-1"></i> Profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="mdi mdi-logout font-size-16 align-middle mr-1"></i> Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </header>

        <div class="vertical-menu">
            <div data-simplebar class="h-100">
                <div id="sidebar-menu">
                    <ul class="metismenu list-unstyled" id="side-menu">
                        @if(Auth()->user()->role == 'admin')
                        <li class="menu-title">Admin</li>
                        <li>
                            <a href="{{ url('admin') }}" class="waves-effect">
                                <div class="d-inline-block icons-sm mr-1"><i class="uim uim-airplay"></i></div>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/post') }}" class=" waves-effect">
                                <div class="d-inline-block icons-sm mr-1"><i class="uim uim-window-grid"></i></div>
                                <span>Posts</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/comment') }}" class=" waves-effect">
                                <div class="d-inline-block icons-sm mr-1"><i class="uim uim-window-grid"></i></div>
                                <span>Comments</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/sharing') }}" class=" waves-effect">
                                <div class="d-inline-block icons-sm mr-1"><i class="uim uim-comment"></i></div>
                                <span>Sharing</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/creation') }}" class=" waves-effect">
                                <div class="d-inline-block icons-sm mr-1"><i class="uim uim-image-v"></i></div>
                                <span>Karya</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/category') }}" class=" waves-effect">
                                <div class="d-inline-block icons-sm mr-1"><i class="uim uim-list-ul"></i></div>
                                <span>Kategori</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('settings') }}" class=" waves-effect">
                                <div class="d-inline-block icons-sm mr-1"><i class="uim uim-object-ungroup"></i></div>
                                <span>Settings</span>
                            </a>
                        </li>
                        @else

                        <li class="menu-title">Menu</li>
                        <li>
                            <a href="{{ url('dashboard') }}" class=" waves-effect">
                                <div class="d-inline-block icons-sm mr-1"><i class="uim uim-airplay"></i></div>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('sharing') }}" class=" waves-effect">
                                <div class="d-inline-block icons-sm mr-1"><i class="uim uim-comment"></i></div>
                                <span>Sharing</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('creation') }}" class=" waves-effect">
                                <div class="d-inline-block icons-sm mr-1"><i class="uim uim-image-v"></i></div>
                                <span>Karya</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('settings') }}" class=" waves-effect">
                                <div class="d-inline-block icons-sm mr-1"><i class="uim uim-object-ungroup"></i></div>
                                <span>Settings</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="main-content">
            <div class="page-content">
                <div class="page-title-box" style="margin-top: -1.5em;">
                    <div class="container-fluid">
                        @yield('header')
                    </div>
                </div>
                <div class="page-content-wrapper">
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                </div>
            </div>

            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            2020 Â© Xoric.
                        </div>
                        <div class="col-sm-6">
                            <div class="text-sm-right d-none d-sm-block">
                                Crafted with <i class="mdi mdi-heart text-danger"></i> by Themesdesign
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

    </div>

    <script src="{{ url('assets/libs/jquery/jquery.min.j') }}s"></script>
    <script src="{{ url('assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ url('assets/libs/metismenu/metisMenu.min.js') }}"></script>
    <script src="{{ url('assets/libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ url('assets/libs/node-waves/waves.min.js') }}"></script>

    <script src="https://unicons.iconscout.com/release/v2.0.1/script/monochrome/bundle.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ url('assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>


    <script src="{{ url('assets/js/app.js') }}"></script>
    <script src="{{ url('assets/libs/parsleyjs/parsley.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            getNotification();
            setInterval(getNotification, 3000);

        })

        function getNotification() {
            $.ajax({
                url: "{{ url('getNotification') }}",
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(res) {
                    $('#notif-area').empty();
                    let total = 0;
                    $.each(res, function(index, val) {
                        $('#notif-area').append("<a href='" + val.cta + "' class='dropdown-item notify-item' data-id='" + val.id + "' onclick='setReadNotif(this)'><span class='align-middle'>" + val.title + "</span></a>");
                        total += 1;
                    });
                    $('#totalNotif').html(total);
                }
            });
        }

        function setReadNotif(obj) {
            $.ajax({
                url: "{{ url('setReadNotif') }}",
                method: 'POST',
                data: {
                    id: $(obj).data('id')
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(res) {}
            });
        }
    </script>

    <script>
        if ('serviceWorker' in navigator && 'PushManager' in window) {
            window.addEventListener('load', function() {
                navigator.serviceWorker.register("{{ url('/sw.js') }}").then(function(
                    registration) {
                    // Registration was successful
                }, function(err) {
                    // registration failed :(
                    console.log('ServiceWorker registration failed: ', err);
                });
            });
        }
    </script>

    @stack('js')

</body>

</html>