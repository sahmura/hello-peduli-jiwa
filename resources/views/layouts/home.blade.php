<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Hello Peduli Jiwa</title>
    <meta content="Hello Peduli Jiwa merupakan platform yang menyediakan berbagai konten tentang kesehatan mental" name="description">
    <meta content="" name="keywords">

    <link rel="apple-touch-icon" sizes="57x57" href="assetsicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assetsicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assetsicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assetsicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assetsicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assetsicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assetsicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assetsicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assetsicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assetsicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assetsicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assetsicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assetsicon/favicon-16x16.png">
    <link rel="manifest" href="assetsicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assetsicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#00807a">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <link href="{{ url('onepagecss/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('onepagecss/assets/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
    <link href="{{ url('onepagecss/assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ url('onepagecss/assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link href="{{ url('onepagecss/assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
    <link href="{{ url('onepagecss/assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ url('onepagecss/assets/vendor/aos/aos.css') }}" rel="stylesheet">

    <link href="{{ url('onepagecss/assets/css/style.css') }}" rel="stylesheet">

    <style>
        .btn-user {
            display: none;
        }

        .btn-user-mobile {
            display: block;
        }

        .mb-mobile-3 {
            margin-bottom: 1em;
        }

        @media (min-width: 992px) {
            .btn-user {
                display: block;
            }

            .btn-user-mobile {
                display: none;
            }

            .mb-mobile-3 {
                margin-bottom: 0;
            }
    </style>

    @stack('css')

</head>

<body>

    <header id="header" class="fixed-top">
        <div class="container d-flex align-items-center">
            <h1 class="logo mr-auto"><a href="{{ url('') }}">Hello Peduli Jiwa</a></h1>
            <nav class="nav-menu d-none d-lg-block">
                <ul>
                    <li class="active"><a href="{{ url('') }}">Home</a></li>
                    <li><a href="{{ url('blog') }}">Blog</a></li>
                    <li><a href="{{ url('sharing') }}">Sharing</a></li>
                    @if(!Auth()->user())
                    <li class="btn-user-mobile"><a href="{{ route('login') }}" class="btn btn-primary mx-3 text-white">Login</a></li>
                    <li class="btn-user-mobile"><a href="{{ route('register') }}" class="btn btn-primary mx-3 text-white">Register</a></li>
                    @else
                    @if(Auth()->user()->role == 'admin')
                    <li class="btn-user-mobile"><a href="{{ url('admin') }}" class="btn btn-primary mx-3 text-white">Dashboard</a></li>
                    @else
                    <li class="btn-user-mobile"><a href="{{ url('dashboard') }}" class="btn btn-primary mx-3 text-white">Dashboard</a></li>
                    @endif
                    @endif
                </ul>
            </nav>

            @if(!Auth()->user())
            <a href="{{ route('login') }}" class="get-started-btn btn-user">Login</a>
            <a href="{{ route('register') }}" class="get-started-btn btn-user">Register</a>
            @else
            @if(Auth()->user()->role == 'admin')
            <a href="{{ url('admin') }}" class="get-started-btn btn-user">Dashboard</a>
            @else
            <a href="{{ url('dashboard') }}" class="get-started-btn btn-user">Dashboard</a>
            @endif
            @endif
        </div>
    </header>

    @yield('hero')

    <main id="main">

        @yield('main')

    </main>

    <footer id="footer">

        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <h3>Hello Peduli Jiwa</h3>
                        <p>
                            Multiplatform dan sahabat bagi semua orang.
                        </p>
                    </div>

                    <div class="col-lg-2 col-md-6 footer-links">
                        <h4>Useful Links</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ url('') }}">Home</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ url('blog') }}">Blog</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ url('sharing') }}">Sharing</a></li>
                            @if(!Auth()->user())
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ url('login') }}">Login</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ url('register') }}">Register</a></li>
                            @else
                            @if(Auth()->user()->role == 'admin')
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ url('admin') }}">Dashboard</a></li>
                            @else
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ url('dashboard') }}">Dashboard</a></li>
                            @endif
                            @endif
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Other Platforms</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ url('') }}">Website</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Book</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Podcast</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ url('blog') }}">Blog</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Content</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-4 col-md-6 footer-newsletter">
                        <h4>Daftar untuk mulai bercerita</h4>
                        <p>Platform ini dikembangkan bagi mereka yang ingin bercerita. Semua pesan akan tersimpan di dalam sistem dan hanya teman-teman yang dapat mengaksesnya.</p>
                        <a class="btn btn-outline-primary" href="{{ url('register') }}">Register</a>
                    </div>

                </div>
            </div>
        </div>

        <div class="container d-md-flex py-4">

            <div class="mr-md-auto text-center text-md-left">
                <div class="copyright">
                    &copy; Copyright Hello Peduli Jiwa. Theme by <strong><span>OnePage</span></strong>. All Rights Reserved
                </div>
                <div class="credits">
                    Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                </div>
            </div>
            <div class="social-links text-center text-md-right pt-3 pt-md-0">
                <a href="https://www.instagram.com/hellopedulijiwa.id/" class="instagram"><i class="bx bxl-instagram"></i></a>
            </div>
        </div>
    </footer>

    <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
    <div id="preloader"></div>

    <script src="{{ url('onepagecss/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('onepagecss/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ url('onepagecss/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    <script src="{{ url('onepagecss/assets/vendor/php-email-form/validate.js') }}"></script>
    <script src="{{ url('onepagecss/assets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
    <script src="{{ url('onepagecss/assets/vendor/counterup/counterup.min.js') }}"></script>
    <script src="{{ url('onepagecss/assets/vendor/venobox/venobox.min.js') }}"></script>
    <script src="{{ url('onepagecss/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ url('onepagecss/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    <script src="{{ url('onepagecss/assets/vendor/aos/aos.js') }}"></script>

    <script src="{{ url('onepagecss/assets/js/main.js') }}"></script>

    <script>
        if ('serviceWorker' in navigator && 'PushManager' in window) {
            window.addEventListener('load', function() {
                navigator.serviceWorker.register("{{ url('/sw.js') }}").then(function(
                    registration) {
                    // Registration was successful
                }, function(err) {
                    // registration failed :(
                    console.log('ServiceWorker registration failed: ', err);
                });
            });
        }
    </script>

    @stack('js')

</body>

</html>