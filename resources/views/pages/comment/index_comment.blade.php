@extends('layouts.main')
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Kelola Komentar</h4>
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Comment</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table table-bordered dt-responsive nowrap" id="dataTable">
                        <thead>
                            <th style="width: 15px;">No</th>
                            <th>Nama</th>
                            <th>Artikel</th>
                            <th>Komentar</th>
                            <th style="width: 20px;"><i class="mdi mdi-apple-keyboard-command"></i></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="dataModal" tabindex="-1" aria-labelledby="dataModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dataModalLabel">Komentar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="dataForm">
                    <input type="hidden" name="reply_to" id="reply_to" value="">
                    <input type="hidden" name="post_id" id="post_id" value="">
                    <h4 id="comment_name"></h4>
                    <p class="lead" id="comment_content"></p>
                    <p id="comment_at"></p>
                    <div id="replies_comment"></div>
                    <div class="form-group mt-3">
                        <label for="reply">Balas komentar</label>
                        <textarea name="reply_comment" id="reply_comment" rows="4" class="form-control"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveButton">Simpan</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    var table = $('#dataTable').DataTable({
        paginate: true,
        info: true,
        sort: true,
        processing: true,
        serverSide: true,
        order: [1, 'ASC'],
        ajax: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('admin/comment') }}",
        },
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false,
                class: 'text-center',
                width: '10px'

            },
            {
                data: 'name',
            },
            {
                data: 'post.title',
                render: function(data, type, row) {
                    if (row.post.title.length > 40) {
                        return row.post.title.substring(0, 40) + '...';
                    } else {
                        return row.post.title;
                    }
                }
            },
            {
                render: function(data, type, row) {
                    if (row.comment.length > 40) {
                        return row.comment.substring(0, 40) + '...';
                    } else {
                        return row.comment;
                    }
                }
            },
            {
                data: 'action'
            }
        ]
    });

    $('#saveButton').on('click', function() {
        var data = $('#dataForm').serialize();

        $.ajax({
            url: "{{ url('admin/comment/reply') }}",
            method: 'POST',
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(res) {
                Swal.fire({
                    title: res.title,
                    text: res.text,
                    icon: res.icon,
                }).then((result) => {
                    $('#dataModal').modal('hide');
                    window.location.reload();
                });
            }
        });
    });

    $(document).on('click', '.deleteButton', function() {
        Swal.fire({
            title: 'Apakah kamu yakin?',
            text: 'Data tidak bisa dikembalikan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#E7472C'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ url('admin/comment/delete') }}",
                    method: 'DELETE',
                    data: {
                        id: $(this).data('id')
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(res) {
                        Swal.fire({
                            title: res.title,
                            text: res.text,
                            icon: res.icon,
                        }).then((result) => {
                            window.location.reload();
                        });
                    }
                });
            }
        });
    })

    $(document).on('click', '.detailButton', function() {
        $.ajax({
            url: "{{ url('admin/comment/detail') }}",
            method: 'POST',
            data: {
                id: $(this).data('id')
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(res) {
                $('#replies_comment').empty();
                console.log(res);
                let replies = '<hr><h5>Reply</h5>';
                $.each(res['reply'], function(index, value) {
                    replies += "<p class='lead'>" + value.comment + "</p>";
                });

                $('#replies_comment').append(replies);
                $('#reply_to').val(res['comment'].id);
                $('#post_id').val(res['comment'].post_id);
                $('#comment_name').html(res['comment'].name);
                $('#comment_content').html(res['comment'].comment);
                $('#comment_at').html('- ' + res['comment'].post.title);
                $('#dataModal').modal('show');
            }
        });
    })
</script>
@endpush