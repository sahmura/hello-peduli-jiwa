@extends('layouts.main')
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Berbagi Karya</h4>
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/creation') }}">Karya</a></li>
        </ol>
    </div>
    <div class="col-md-4">
        <div class="float-right d-none d-md-block">
            <button class="btn btn-light btn-rounded dropdown-toggle" type="button" onclick="neworupdate(this)" data-tipe="new">
                <i class="mdi mdi mdi-plus-thick mr-1"></i> Tambah karya baru
            </button>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h5 class="header-title mb-4">Karya terbaru</h5>
                <div id="listKarya" class="row"></div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="newupdatemodal" tabindex="-1" aria-labelledby="newupdatemodalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newupdatemodalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="newupdateform" enctype="multipart/form-data">
                    <input type="hidden" name="id" id="creation_id" value="">
                    <div class="form-group">
                        <label for="name">Nama karya</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nama karya" required>
                    </div>
                    <div class="form-group">
                        <label for="desc">Deskripsi karya</label>
                        <textarea name="desc" id="desc" rows="3" class="form-control" placeholder="Deskripsi karya bisa diisi dengan penjelasan karya atau lainnya" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="file">File karya</label>
                        <input type="file" name="file" id="file" class="form-control" placeholder="File karya">
                        <small>Karya bisa berupa gambar</small>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="simpanbutton" onclick="submit()">Simpan data</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(document).ready(function() {
        refresh_data();
    });

    $('#btnFilter').on('click', function() {
        refresh_data();
    });

    function refresh_data() {
        $.ajax({
            url: "{{ url('creation/lists') }}",
            method: 'POST',
            data: {
                id: "{{ Auth()->user()->id }}"
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(res) {
                var listkarya = '';
                if (res.length > 0) {
                    $.each(res, function(idx, val) {
                        listkarya += '<div class="col-md-4 mb-mobile-3">' +
                            '<div class="card" style="box-shadow: 1px 2px 5px 0px rgba(0,0,0,0.4)">' +
                            '<div class="card-body"> ' +
                            '<div style="width: 100%; height: 215px" class="overflow-hidden d-flex align-items-center">' +
                            '<img src="{{url("creationfile")}}/' + val.file + '" alt="' + val.name + '" class="img-fluid">' +
                            '</div>' +
                            '<br>' +
                            '<a href="{{ url("creation/detail/") }}/' + val.id + '" class="card-title">' +
                            '<h6 class="text-primary">' + val.name.substr(0, 20) + '...</h6>' +
                            '</a>' +
                            '</div>' +
                            '<div class="card-footer">' +
                            '<div class="row">' +
                            '<div class="col-6">' +
                            '<button data-id="' + val.id + '" data-name="' + val.name + '" data-desc="' + val.desc + '" class="btn btn-warning btn-block btn-sm" onclick="neworupdate(this)"><i class="mdi mdi-pencil-outline"></i></button>' +
                            '</div>' +
                            '<div class="col-6">' +
                            '<button data-id="' + val.id + '" class="btn btn-danger btn-block btn-sm deleteButton"><i class="mdi mdi-trash-can-outline"></i></button>' +
                            '</div></div></div>' +
                            '</div>' +
                            '</div>';
                    });
                } else {
                    listkarya = '<div class="col-md-12 text-center"><p class="lead"><b>Tidak ada data</b></p></div>';
                }

                $('#listKarya').append(listkarya);
            }
        });
    }

    $(document).on('click', '.showButton', function() {

    });

    $(document).on('click', '.deleteButton', function() {
        Swal.fire({
            title: 'Apakah kamu yakin?',
            text: 'Data tidak bisa dikembalikan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#E7472C'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ url('creation/delete') }}",
                    method: 'DELETE',
                    data: {
                        id: $(this).data('id')
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(res) {
                        Swal.fire({
                            title: res.title,
                            text: res.text,
                            icon: res.icon,
                        }).then((result) => {
                            window.location.reload();
                        });
                    }
                });
            }
        });
    });

    function neworupdate(obj) {
        var tipe = $(obj).data('tipe');
        $("#newupdateform")[0].reset();

        if (tipe == 'new') {
            $('#newupdatemodalLabel').html('Tambah karya');
        } else {
            $('#creation_id').val($(obj).data('id'));
            $('#name').val($(obj).data('name'));
            $('#desc').val($(obj).data('desc'));
            $('#newupdatemodalLabel').html('Edit karya');
        }

        $('#newupdatemodal').modal('show');
    }

    function submit() {

        var data = new FormData();
        var dataFile = $('#file')[0].files;

        data.append('name', $('#name').val());
        data.append('desc', $('#desc').val());

        if (dataFile.length > 0) {
            data.append('creation', dataFile[0]);
            var processData = false;
        } else {
            data.append('creation', '');
            var processData = true;
        }

        if ($('#creation_id').val() == '') {
            var url = "{{ url('creation/create') }}";
        } else {
            data.append('id', $('#creation_id').val());
            var url = "{{ url('creation/update') }}";
        }

        $.ajax({
            url: url,
            data: data,
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            contentType: false,
            processData: false,
            success: function(res) {
                Swal.fire({
                    title: res.title,
                    text: res.text,
                    icon: res.icon,
                }).then((result) => {
                    window.location.reload();
                });
            }
        });
    }
</script>
@endpush