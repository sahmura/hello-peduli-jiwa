@extends('layouts.home')

@section('hero')
<section id="hero" class="d-flex align-items-center">
    <div class="container position-relative" data-aos="fade-up" data-aos-delay="100">
        <div class="row justify-content-center pt-5">
            <div class="col-xl-7 col-lg-9 text-center">
                <h1>{{ $data->name }}</h1>
                <h5 class="mt-3">Diterbitkan pada {{ \Carbon\Carbon::parse($data->created_at)->format('d-m-Y H:i') }}</h5>
                <h5>Oleh {{ ($data->user) ? $data->user->name : 'Tidak bernama' }}</h5>
            </div>
        </div>

    </div>
</section>
@endsection
@section('main')
<section id="about" class="about">
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="header-title mb-4">{{ $data->name }}</h5>
                        <div id="listKarya" class="row d-flex align-items-center justify-content-center">
                            <div class="col-md-12">
                                <div style="height: 400px;" class="overflow-auto">
                                    @if($data->extension != 'pdf')
                                    <img src="{{ url('creationfile/' . $data->file) }}" alt="{{ $data->name }}" class="img-fluid">
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <p class="lead">
                                    {{ $data->desc }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection