@extends('layouts.home')

@section('hero')
<section id="hero" class="d-flex align-items-center">
    <div class="container position-relative" data-aos="fade-up" data-aos-delay="100">
        <div class="row justify-content-center pt-5">
            <div class="col-xl-7 col-lg-9 text-center">
                <h1>Karya Member Hello Peduli Jiwa</h1>
            </div>
        </div>
    </div>
</section>
@endsection

@section('main')
<section id="about" class="about" style="margin-top: -2em;">
    <div class="container" data-aos="fade-up">
        <div class="row mb-5">
            <div class="col-md-12">
                <div class="input-group">
                    <input id="cariform" type="text" class="form-control form-control-lg" placeholder="Cari karya" aria-label="Cari karya" aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary px-5" type="button" onclick="search()">Cari</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-4">Karya terbaru</h5>
                @if($data->isNotEmpty())
                <div class="row">
                    @foreach($data as $art)
                    <div class="col-md-4 mb-mobile-3">
                        <div class="card">
                            <div class="card-body">
                                <div style="width: 100%; height: 215px" class="overflow-hidden">
                                    @if($art->extension != 'pdf')
                                    <img src="{{ url('creationfile/' . $art->file) }}" alt="{{ $art->name }}" class="img-fluid">
                                    @endif
                                </div>
                                <br>
                                <a href="{{ url('look/' . $art->id) }}" class="card-title">
                                    <h6 class="text-primary">{{ Str::limit($art->name,40) }}</h6>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @else
                <h6>Belum ada karya</h6>
                @endif
            </div>
            <div class="col-md-12 mt-5 d-flex justify-content-center">
                {{ $data->links() }}
            </div>
        </div>
    </div>
</section>

<section id="cta" class="cta">
    <div class="container" data-aos="zoom-in">
        <div class="text-center">
            <h3>Ingin mengirim karya?</h3>
            <p>Kami menerima semua karya digital baik berupa gambar, foto, maupun dokumen misalnya berisi cerita pendek, puisi, dan lain sebagainya</p>
            <a class="cta-btn" href="{{ url('creation') }}">Kirim karya sekarang</a>
        </div>
    </div>
</section>
@endsection