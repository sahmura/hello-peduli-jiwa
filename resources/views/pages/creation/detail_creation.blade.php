@extends('layouts.main')
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Berbagi Karya</h4>
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/creation') }}">Karya</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/creation/' . $data->id) }}">{{ $data->name }}</a></li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h5 class="header-title mb-4">{{ $data->name }}</h5>
                <div id="listKarya" class="row d-flex align-items-center justify-content-center">
                    <div class="col-md-12">
                        <div style="height: 400px;" class="overflow-auto">
                            @if($data->extension != 'pdf')
                            <img src="{{ url('creationfile/' . $data->file) }}" alt="{{ $data->name }}" class="img-fluid">
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <p class="lead">
                            {{ $data->desc }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection