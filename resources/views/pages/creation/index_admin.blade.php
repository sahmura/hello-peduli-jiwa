@extends('layouts.main')
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Kelola Karya</h4>
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Creation</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table table-bordered dt-responsive nowrap" id="dataTable">
                        <thead>
                            <th style="width: 15px;">No</th>
                            <th>Nama</th>
                            <th>Pengirim</th>
                            <th style="width: 20px;"><i class="mdi mdi-apple-keyboard-command"></i></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="dataModal" tabindex="-1" aria-labelledby="dataModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dataModalLabel">Detail Karya</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div style="height: 400px;" class="overflow-auto" id="det_karya_file">
                        </div>
                    </div>
                    <div class="col-md-12 mt-3">
                        <h3 id="det_karya_nama"></h3>
                        <p id="det_karya_from"></p>
                        <hr>
                        <p id="det_karya_desc"></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    var table = $('#dataTable').DataTable({
        paginate: true,
        info: true,
        sort: true,
        processing: true,
        serverSide: true,
        order: [1, 'ASC'],
        ajax: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('admin/creation') }}",
        },
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false,
                class: 'text-center',
                width: '10px'

            },
            {
                data: 'name',
            },
            {
                data: 'user.name',
            },
            {
                data: 'action'
            }
        ]
    });

    $(document).on('click', '.detailButton', function() {
        get_karya($(this).data('id'));
        $('#dataModal').modal('show');
    });

    function get_karya(id) {
        $.ajax({
            url: "{{ url('admin/creation/get') }}",
            method: 'POST',
            data: {
                id: id
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(res) {
                $('#det_karya_nama').html(res.name);
                $('#det_karya_from').html(res.user.name);
                $('#det_karya_desc').html(res.desc);

                if (res.extension != 'pdf') {
                    var file = "<img src='{{ url('creationfile') }}/" + res.file + "' alt='" + res.name + "' class='img-fluid'>";
                }

                $("#det_karya_file").append(file);
            }
        });
    }

    $(document).on('click', '.deleteButton', function() {
        Swal.fire({
            title: 'Apakah kamu yakin?',
            text: 'Data tidak bisa dikembalikan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#E7472C'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ url('admin/creation/delete') }}",
                    method: 'DELETE',
                    data: {
                        id: $(this).data('id')
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(res) {
                        Swal.fire({
                            title: res.title,
                            text: res.text,
                            icon: res.icon,
                        }).then((result) => {
                            $('#dataModal').modal('hide');
                            window.location.reload();
                        });
                    }
                });
            }
        });
    })
</script>
@endpush