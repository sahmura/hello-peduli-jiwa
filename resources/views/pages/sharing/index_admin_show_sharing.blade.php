@extends('layouts.main')
@push('css')
<style>
    .bubblechat {
        position: relative;
        max-width: 30em;

        background-color: #fff;
        padding: 1.125em 1.5em;
        font-size: 1em;
        border-radius: 0.5rem;
        box-shadow: 0 0.125rem 0.25rem rgba(0, 0, 0, .1), 0 0.0625rem 0.125rem rgba(0, 0, 0, .1);
    }

    .bubblechat::before {
        content: '';
        position: absolute;
        width: 0;
        height: 0;
        bottom: 100%;
        left: 1.5em;
        border: .75rem solid transparent;
        border-top: none;

        border-bottom-color: #fff;
        filter: drop-shadow(0 -0.0625rem 0.0625rem rgba(0, 0, 0, .1));
    }

    .bubblechatme {
        position: relative;
        max-width: 30em;

        background-color: #fff;
        padding: 1.125em 1.5em;
        font-size: 1em;
        border-radius: 0.5rem;
        box-shadow: 0 0.125rem 0.25rem rgba(0, 0, 0, .1), 0 0.0625rem 0.125rem rgba(0, 0, 0, .1);
    }

    .bubblechatme::before {
        content: '';
        position: absolute;
        width: 0;
        height: 0;
        bottom: 100%;
        right: 1.5em;
        border: .75rem solid transparent;
        border-top: none;

        border-bottom-color: #fff;
        filter: drop-shadow(0 -0.0625rem 0.0625rem rgba(0, 0, 0, .1));
    }
</style>
@endpush
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Sharing</h4>
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/admin/sharing') }}">Sharing</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/admin/sharing/' . $detail->id) }}">{{ Str::limit($detail->topic, 20) }}</a></li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h6 class="header-title mb-4">Detail</h6>
                <h4>{{ $detail->topic }}</h4>
                <p class="lead">{{ $detail->description }}</p>
                <br>
                <p>Diposting oleh {{ $detail->user->name }}</p>
            </div>
            <div class="card-footer">
                <p>Pada {{ \Carbon\Carbon::parse($detail->created_at)->format('d/m/Y H:i') }}</p>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="card">
            <div class="card-body">
                <h6 class="header-title mb-4">Sharing</h6>
                <div class="row">
                    <div class="col-12">
                        <div style="height: 400px;" class="overflow-auto">
                            @foreach($data as $chat)
                            @if($chat->user_id == Auth()->user()->id)
                            <div class="bubblechatme my-4 mx-2 ml-auto bg-primary text-white">{{ $chat->message }}</div>
                            @else
                            <div class="bubblechat my-4 mx-2">{{ $chat->message }}</div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="col-12">
                        <form id="replyform">
                            <input type="hidden" name="topic_id" id="topic_id" value="{{ $detail->id }}">
                            <input type="hidden" name="to_id" id="to_id" value="{{ $detail->user_id }}">
                            <div class="form-group">
                                <textarea name="message" id="message" rows="2" class="form-control" placeholder="Balas di sini"></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success btn-submit float-right px-4" type="button"><i class="mdi mdi-send"></i> Kirim</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $('.btn-submit').on('click', function() {
        var data = $('#replyform').serialize();
        $.ajax({
            url: "{{ url('admin/sharing/reply') }}",
            method: 'POST',
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(res) {
                Swal.fire({
                    title: res.title,
                    text: res.text,
                    icon: res.icon,
                }).then((result) => {
                    window.location.reload();
                });
            }
        });
    })
</script>
@endpush