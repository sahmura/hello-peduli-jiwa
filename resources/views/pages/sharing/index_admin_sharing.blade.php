@extends('layouts.main')
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Sharing</h4>
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/admin/sharing') }}">Sharing</a></li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h5 class="header-title mb-4">Topik terbaru</h5>
                <div class="row d-flex align-items-end my-4">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="tgl_awal">Tanggal awal</label>
                            <input type="date" name="tgl_awal" id="tgl_awal" class="form-control datepicker" placeholder="Filter tanggal" value="{{ date('Y-m-01') }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="tgl_akhir">Tanggal akhir</label>
                            <input type="date" name="tgl_akhir" id="tgl_akhir" class="form-control datepicker" placeholder="Filter tanggal" value="{{ date('Y-m-d') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <button class="btn btn-primary btn-block wafes-effect" id="btnFilter">Filter topik</button>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-borderless" id="datatable">
                        <thead>
                            <th style="width: 15px;">No</th>
                            <th>Dari</th>
                            <th>Topic</th>
                            <th style="width: 20px;"><i class="mdi mdi-apple-keyboard-command"></i></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(document).ready(function() {
        refresh_data();
    });

    $('#btnFilter').on('click', function() {
        refresh_data();
    });

    function refresh_data() {
        var table = $('#datatable').DataTable({
            paginate: true,
            info: true,
            sort: true,
            processing: true,
            serverSide: true,
            destroy: true,
            order: [1, 'ASC'],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('admin/sharing/list_admin') }}",
                method: 'post',
                data: {
                    tgl_awal: $('#tgl_awal').val(),
                    tgl_akhir: $('#tgl_akhir').val(),
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    class: 'text-center',
                    width: '10px'

                },
                {
                    data: 'user.name',
                },
                {
                    data: 'topic',
                },
                {
                    data: 'action'
                }
            ]
        });
    }

    $(document).on('click', '.showButton', function() {
        window.location.href = "{{ url('admin/sharing/detail/') }}" + '/' + $(this).data('id');
    });

    $(document).on('click', '.deleteButton', function() {
        Swal.fire({
            title: 'Apakah kamu yakin?',
            text: 'Data tidak bisa dikembalikan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#E7472C'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ url('admin/sharing/delete') }}",
                    method: 'DELETE',
                    data: {
                        id: $(this).data('id')
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(res) {
                        Swal.fire({
                            title: res.title,
                            text: res.text,
                            icon: res.icon,
                        }).then((result) => {
                            window.location.reload();
                        });
                    }
                });
            }
        });
    });
</script>
@endpush