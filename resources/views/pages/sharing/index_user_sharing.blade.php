@extends('layouts.main')
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Sharing</h4>
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/sharing') }}">Sharing</a></li>
        </ol>
    </div>
    <div class="col-md-4">
        <div class="float-right d-none d-md-block">
            <button class="btn btn-light btn-rounded dropdown-toggle" type="button" onclick="neworupdate(this)" data-tipe="new">
                <i class="mdi mdi mdi-plus-thick mr-1"></i> Buat topic baru
            </button>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h5 class="header-title mb-4">Topik terbaru</h5>
                <div class="row d-flex align-items-end my-4">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="tgl_awal">Tanggal awal</label>
                            <input type="date" name="tgl_awal" id="tgl_awal" class="form-control datepicker" placeholder="Filter tanggal" value="{{ date('Y-m-01') }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="tgl_akhir">Tanggal akhir</label>
                            <input type="date" name="tgl_akhir" id="tgl_akhir" class="form-control datepicker" placeholder="Filter tanggal" value="{{ date('Y-m-d') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <button class="btn btn-primary btn-block wafes-effect" id="btnFilter">Filter topik</button>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-borderless" id="datatable">
                        <thead>
                            <th style="width: 15px;">No</th>
                            <th>Dari</th>
                            <th>Topic</th>
                            <th style="width: 20px;"><i class="mdi mdi-apple-keyboard-command"></i></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="newupdatemodal" tabindex="-1" aria-labelledby="newupdatemodalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newupdatemodalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="newupdateform">
                    <input type="hidden" name="id" id="topic_id" value="">
                    <div class="form-group">
                        <label for="topic">Topic</label>
                        <input type="text" name="topic" id="topic" class="form-control" placeholder="Judul topic" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Detail topic</label>
                        <textarea name="description" id="description" rows="3" class="form-control" placeholder="Detail topic" required></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="simpanbutton" onclick="submit()">Simpan data</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(document).ready(function() {
        refresh_data();
    });

    $('#btnFilter').on('click', function() {
        refresh_data();
    });

    function refresh_data() {
        var table = $('#datatable').DataTable({
            paginate: true,
            info: true,
            sort: true,
            processing: true,
            serverSide: true,
            destroy: true,
            order: [1, 'ASC'],
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ url('sharing/lists') }}",
                method: 'post',
                data: {
                    tgl_awal: $('#tgl_awal').val(),
                    tgl_akhir: $('#tgl_akhir').val(),
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false,
                    class: 'text-center',
                    width: '10px'

                },
                {
                    data: 'user.name',
                },
                {
                    data: 'topic',
                },
                {
                    data: 'action'
                }
            ]
        });
    }

    $(document).on('click', '.showButton', function() {
        window.location.href = "{{ url('sharing/detail/') }}" + '/' + $(this).data('id');
    });

    $(document).on('click', '.deleteButton', function() {
        Swal.fire({
            title: 'Apakah kamu yakin?',
            text: 'Data tidak bisa dikembalikan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#E7472C'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ url('sharing/delete') }}",
                    method: 'DELETE',
                    data: {
                        id: $(this).data('id')
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(res) {
                        Swal.fire({
                            title: res.title,
                            text: res.text,
                            icon: res.icon,
                        }).then((result) => {
                            window.location.reload();
                        });
                    }
                });
            }
        });
    });

    function neworupdate(obj) {
        var tipe = $(obj).data('tipe');
        $("#newupdateform")[0].reset();

        if (tipe == 'new') {
            $('#newupdatemodalLabel').html('Tambah topic');
        } else {
            $('#topic_id').val($(obj).data('id'));
            $('#topic').val($(obj).data('topic'));
            $('#description').val($(obj).data('description'));
            $('#newupdatemodalLabel').html('Edit topic');
        }

        $('#newupdatemodal').modal('show');
    }

    function submit() {

        var data = $("#newupdateform").serialize();

        if ($('#topic_id').val() == '') {
            var url = "{{ url('sharing/store') }}";
            var method = 'POST';
        } else {
            var url = "{{ url('sharing/update') }}";
            var method = 'PUT';
        }

        $.ajax({
            url: url,
            data: data,
            method: method,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(res) {
                Swal.fire({
                    title: res.title,
                    text: res.text,
                    icon: res.icon,
                }).then((result) => {
                    window.location.reload();
                });
            }
        });
    }
</script>
@endpush