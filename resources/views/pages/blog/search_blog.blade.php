@extends('layouts.home')

@section('hero')
<section id="hero" class="d-flex align-items-center">
    <div class="container position-relative" data-aos="fade-up" data-aos-delay="100">
        <div class="row justify-content-center pt-5">
            <div class="col-xl-7 col-lg-9 text-center">
                <h1>Artikel Hello Peduli Jiwa</h1>
            </div>
        </div>

        @if($populers->isNotEmpty())
        <div class="row mt-5">
            <div class="col-md-12">
                <h2 class="mb-4">Artikel Populer dalam {{ ucfirst($type) }}</h2>
                <div class="owl-carousel owl-theme">
                    @foreach($populers as $populer)
                    <div>
                        <div style="width: 100%; height: 400px">
                            <div class="card">
                                <div class="card-body">
                                    <div style="width: 100%; height: 215px" class="overflow-hidden">
                                        <img src="{{ url('cover_post/' . $populer->cover) }}" alt="{{ $populer->title }}" class="img-fluid">
                                    </div>
                                    <br>
                                    <a href="{{ url('read/' . $populer->id . '/' . $populer->slang) }}">
                                        <h6 class="text-primary">{{ Str::limit($populer->title,40) }}</h6>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif
    </div>
</section>
@endsection
@section('main')
<section id="about" class="about">
    <div class="container" data-aos="fade-up">
        <div class="row mb-5">
            <div class="col-md-12">
                <div class="input-group">
                    <input id="cariform" type="text" class="form-control form-control-lg" placeholder="Cari artikel" aria-label="Cari artikel" aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary px-5" type="button" onclick="search()">Cari</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5 class="mb-4">Artikel dalam {{ ucfirst($type) }}</h5>
                @if($posts->isNotEmpty())
                <div class="row">
                    @foreach($posts as $post)
                    <div class="col-md-4 mb-mobile-3">
                        <div class="card">
                            <div class="card-body">
                                <div style="width: 100%; height: 215px" class="overflow-hidden">
                                    <img src="{{ url('cover_post/' . $post->cover) }}" alt="{{ $post->title }}" class="img-fluid">
                                </div>
                                <br>
                                <a href="{{ url('read/' . $post->id . '/' . $post->slang) }}" class="card-title">
                                    <h6 class="text-primary">{{ Str::limit($post->title,40) }}</h6>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @else
                <h6>Belum ada artikel</h6>
                @endif
            </div>
            <div class="col-md-12 mt-5 d-flex justify-content-center">
                {{ $posts->links() }}
            </div>
        </div>
    </div>
</section>

<section id="cta" class="cta">
    <div class="container" data-aos="zoom-in">
        <div class="text-center">
            <h3>Mulai bercerita</h3>
            <p>Kami menerima semua cerita dan curhatan teman-teman. Baik senang maupun sedih, baik ketika bersama teman maupun sedang sendiri. Kami ada untuk menemani.</p>
            <a class="cta-btn" href="{{ url('sharing') }}">Mulai bercerita</a>
        </div>
    </div>
</section>
@endsection

@push('js')

<script>
    $(document).ready(function() {
        $(".owl-carousel").owlCarousel({
            center: true,
            items: 3,
            loop: true,
            margin: 15,
            autoplay: true,
            autoplayTimeout: 4000,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 3,
                    nav: false
                }
            }
        });
    });

    function search() {
        window.location.href = "{{ url('blog/search/') }}" + '/' + $('#cariform').val();
    }
</script>
@endpush