@extends('layouts.home')

@section('hero')
<section id="hero" class="d-flex align-items-center" style="background-image: url('{{ $cover }}') !important;">
    <div class="container position-relative" data-aos="fade-up" data-aos-delay="100">
        <div class="row justify-content-center pt-5">
            <div class="col-xl-7 col-lg-9 text-center">
                <h1>{{ $data->title }}</h1>
                <h5 class="mt-3">Diterbitkan pada {{ \Carbon\Carbon::parse($data->created_at)->format('d-m-Y H:i') }}</h5>
                <h5>Dalam kategori {{ $data->category->name ?? 'Tidak berkategori' }}</h5>
            </div>
        </div>

    </div>
</section>
@endsection
@section('main')
<section id="about" class="about">
    <div class="container" data-aos="fade-up">
        <div class="row">
            <div class="col-md-8 mb-mobile-3">
                <p style='font-size: 20px'>{!! $data->content !!}</p>
                <div class="container mt-5" data-aos='fade-up'>
                    <h4>Komentar ({{ count($comments) }})</h4>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            @if(count($comments) != 0)
                            @foreach($comments as $comment)
                            <div class="row">
                                <div class="col-sm-1 mb-mobile-3">
                                    <img class="rounded-circle" src="https://ui-avatars.com/api/?name={{ str_replace(' ', '+', $comment->name) }}" alt="{{ $comment->name }}" height="40px">
                                </div>
                                <div class="col-sm-6">
                                    <h6 style="font-weight: 600;">{{ $comment->name }}</h6>
                                    <p>{{ $comment->comment }}</p>
                                </div>
                            </div>
                            @foreach($replies as $reply)
                            @if($reply->reply_to == $comment->id)
                            <div class="row">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-1 mb-mobile-3">
                                    <img class="rounded-circle" src="https://ui-avatars.com/api/?name={{ str_replace(' ', '+', $reply->name) }}" alt="{{ $reply->name }}" height="40px">
                                </div>
                                <div class="col-sm-6">
                                    <h6 style="font-weight: 600;">{{ $reply->name }}</h6>
                                    <p>{{ $reply->comment }}</p>
                                </div>
                            </div>
                            @endif
                            @endforeach
                            @endforeach
                            @else
                            <h6 class="text-center">Belum ada komentar. Jadilah yang pertama.</h6>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1 mb-mobile-3"></div>
            <div class="col-md-3 mb-mobile-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Kategori</h5>
                        <ul class="list-group">
                            @foreach($categories as $category)
                            <li class="list-group-item"><a href="{{ url('blog/search/category/' . $category->id) }}">{{ $category->name }}</a></li>
                            @endforeach
                            <li class="list-group-item"><a href="{{ url('blog/search/category/0') }}">Tidak berkategori</a></li>
                        </ul>
                    </div>
                </div>
                <div class="card mt-3">
                    <div class="card-body">
                        <h5 class="card-title">Arsip</h5>
                        <ul class="list-group">
                            @foreach($archives as $archive)
                            <li class="list-group-item"><a href="{{ url('blog/search/archive/' . $archive->archive) }}">{{ $archive->archive }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="about" class="about">

</section>

<section id="cta" class="cta">
    <div class="container" data-aos="zoom-in">
        <div class="text-center">
            <div class="row d-flex justify-content-center">
                <div class="col-md-6">
                    <h3>Tambah Komentar</h3>
                    <form id="commentform">
                        <input type="hidden" name="post_id" id="post_id" value="{{ $data->id }}">
                        <div class="form-group">
                            <label for="name_comment" class="text-white">Nama</label>
                            <input type="text" name="name" id="name_comment" class="form-control @if(Auth()->user()) readonly @endif" placeholder="Nama" value="{{ Auth()->user()->name ?? '' }}" @if(Auth()->user()) readonly @endif>
                        </div>
                        <div class="form-group">
                            <label for="comment" class="text-white">Komentar</label>
                            <textarea name="comment" id="comment" rows="5" class="form-control" placeholder="Isi komentar"></textarea>
                        </div>
                    </form>
                    <a id="add-comment-btn" class="cta-btn" style="cursor:pointer">Tambahkan Komentar</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('js')

<script>
    $('#add-comment-btn').on('click', function() {
        var data = $('#commentform').serialize();
        Swal.fire({
            title: 'Tambahkan komentar',
            text: '',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#4487CE',
            confirmButtonText: 'Tambahkan'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ url('comment/add') }}",
                    method: 'POST',
                    data: data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(res) {
                        Swal.fire({
                            title: res.title,
                            text: res.text,
                            icon: res.icon,
                        }).then((result) => {
                            window.location.reload();
                        });
                    }
                });
            }
        });
    });
</script>
@endpush