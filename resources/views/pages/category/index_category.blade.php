@extends('layouts.main')
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Kelola Kategori</h4>
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Category</li>
        </ol>
    </div>
    <div class="col-md-4">
        <div class="float-right d-none d-md-block">
            <div class="dropdown">
                <button class="btn btn-light btn-rounded dropdown-toggle" type="button" id="addButton">
                    <i class="mdi mdi-plus-thick mr-1"></i> Tambah Data
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table table-bordered dt-responsive nowrap" id="dataTable">
                        <thead>
                            <th style="width: 15px;">No</th>
                            <th>Nama</th>
                            <th style="width: 20px;"><i class="mdi mdi-apple-keyboard-command"></i></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="dataModal" tabindex="-1" aria-labelledby="dataModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dataModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="dataForm">
                    <input type="hidden" name="id" id="id" value="">
                    <div class="form-group">
                        <label for="name">Nama kategori</label>
                        <input type="text" name="name" id="name" class="form-control" required placeholder="Nama kategori">
                    </div>
                    <div class="form-group">
                        <label for="description">Deskripsi kategori</label>
                        <textarea name="description" id="description" rows="4" class="form-control" placeholder="Deskripsi kategory"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveButton">Simpan</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    var table = $('#dataTable').DataTable({
        paginate: true,
        info: true,
        sort: true,
        processing: true,
        serverSide: true,
        order: [1, 'ASC'],
        ajax: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('admin/category') }}",
        },
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false,
                class: 'text-center',
                width: '10px'

            },
            {
                data: 'name',
            },
            {
                data: 'action'
            }
        ]
    });

    $('#addButton').on('click', function() {
        $('#dataModalLabel').html('Tambah data');
        $('#dataForm')[0].reset();
        $('#saveButton').addClass('btn-primary');
        $('#saveButton').removeClass('btn-warning');
        $('#dataModal').modal('show');
    });

    $('#saveButton').on('click', function() {
        var id = $('#id').val();
        var data = $('#dataForm').serialize();

        if (id == '') {
            var url = "{{ url('admin/category/create') }}";
        } else {
            var url = "{{ url('admin/category/update') }}";
        }

        $.ajax({
            url: url,
            method: 'POST',
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(res) {
                Swal.fire({
                    title: res.title,
                    text: res.text,
                    icon: res.icon,
                }).then((result) => {
                    $('#dataModal').modal('hide');
                    window.location.reload();
                });
            }
        });
    });

    $(document).on('click', '.editButton', function() {
        $('#dataModalLabel').html('Sunting data');
        $('#saveButton').removeClass('btn-primary');
        $('#saveButton').addClass('btn-warning');
        $('#dataForm')[0].reset();
        $('#id').val($(this).data('id'));
        $('#name').val($(this).data('name'));
        $('#description').val($(this).data('desc'));
        $('#dataModal').modal('show');
    });

    $(document).on('click', '.deleteButton', function() {
        Swal.fire({
            title: 'Apakah kamu yakin?',
            text: 'Data tidak bisa dikembalikan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#E7472C'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ url('admin/category/delete') }}",
                    method: 'DELETE',
                    data: {
                        id: $(this).data('id')
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(res) {
                        Swal.fire({
                            title: res.title,
                            text: res.text,
                            icon: res.icon,
                        }).then((result) => {
                            $('#dataModal').modal('hide');
                            window.location.reload();
                        });
                    }
                });
            }
        });
    })
</script>
@endpush