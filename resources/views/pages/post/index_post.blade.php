@extends('layouts.main')
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Kelola Artikel</h4>
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Post</li>
        </ol>
    </div>
    <div class="col-md-4">
        <div class="float-right d-none d-md-block">
            <div class="dropdown">
                <button class="btn btn-light btn-rounded dropdown-toggle" type="button" id="addButton">
                    <i class="mdi mdi-plus-thick mr-1"></i> Artikel baru
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered dt-responsive nowrap" id="dataTable">
                        <thead>
                            <th style="width: 15px;">No</th>
                            <th>Artikel</th>
                            <th>Kategori</th>
                            <th>Status</th>
                            <th style="width: 20px;"><i class="mdi mdi-apple-keyboard-command"></i></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    var table = $('#dataTable').DataTable({
        paginate: true,
        info: true,
        sort: true,
        processing: true,
        serverSide: true,
        order: [1, 'ASC'],
        ajax: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('admin/post') }}",
        },
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false,
                class: 'text-center',
                width: '10px'

            },
            {
                data: 'artikel',
            },
            {
                data: 'category',
            },
            {
                data: 'type',
            },
            {
                data: 'action'
            }
        ]
    });

    $('#addButton').on('click', function() {
        window.location.href = "{{ url('admin/post/new') }}"
    });

    $(document).on('click', '.editButton', function() {
        window.location.href = "{{ url('admin/post') }}" + '/' + $(this).data('id') + '/edit';
    });

    $(document).on('click', '.deleteButton', function() {
        Swal.fire({
            title: 'Apakah kamu yakin?',
            text: 'Data tidak bisa dikembalikan',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#E7472C'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ url('admin/post/delete') }}",
                    method: 'DELETE',
                    data: {
                        id: $(this).data('id')
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(res) {
                        Swal.fire({
                            title: res.title,
                            text: res.text,
                            icon: res.icon,
                        }).then((result) => {
                            window.location.reload();
                        });
                    }
                });
            }
        });
    })
</script>
@endpush