@extends('layouts.main')
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Tambah Artikel</h4>
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ url('/admin/post') }}">Post</a></li>
            <li class="breadcrumb-item active">Edit Post</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<form action="" id="dataForm" enctype="multipart/form-data" data-parsley-validate action="">
    <input type="hidden" name="id" id="id" value="{{ $data->id }}">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Judul artikel</label>
                        <input type="text" class="form-control" name="title" id="title" required placeholder="Judul Artikel" value="{{ $data->title }}">
                    </div>
                    <div class="form-group">
                        <label for="content">Konten</label>
                        <textarea name="content" id="content" placeholder="Konten"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <button class="btn btn-primary btn-block waves-effect" id="publishButton">Terbitkan</button>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-outline-warning btn-block waves-effect" id="draftButton">Simpan draft</button>
                    </div>
                    <div class="form-group">
                        <label for="category_id">Kategori</label>
                        <select name="category_id" id="category_id" class="form-control" required>
                            <option value="0">Tidak berkategori</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}" @if($category->id == $data->category_id) selected='selected' @endif>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="cover">Cover artikel</label>
                        <div class="embed-responsive embed-responsive-16by9 mb-3">
                            <div class="embed-responsive-item">
                                <img class="img-fluid" src="{{ url('cover_post/' . $data->cover) }}" alt="">
                            </div>
                        </div>
                        <input type="file" class="form-control" placeholder="Cover artikel" name="cover" id="cover">
                    </div>
                    <div class="form-group">
                        <label for="tags">Tags</label>
                        <input type="text" name="tags" id="tags" placeholder="Kesehatan, mental, dsb" class="form-control" required value="{{ $data->tags }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@push('js')
<script src="{{ url('assets/libs/ckeditor4/ckeditor.js') }}"></script>
<script>
    $(document).ready(function() {
        CKEDITOR.replace('content');
        CKEDITOR.instances['content'].setData("{!! trim(preg_replace('/\s\s+/', ' ', $data->content)) !!}");
        $('#dataForm').parsley();
    });

    $('#publishButton').on('click', function(e) {
        if ($('#dataForm').parsley().isValid()) {
            e.preventDefault()
            var data = new FormData();
            var image = $('#cover')[0].files;

            if (image.length > 0) {
                data.append('cover', image[0]);
            } else {
                data.append('cover', 'not');
            }

            data.append('id', $('#id').val());
            data.append('title', $('#title').val());
            data.append('content', CKEDITOR.instances['content'].getData());
            data.append('category_id', $('#category_id option:selected').val());
            data.append('tags', $('#tags').val());
            data.append('type', 'PUBLISH');

            Swal.fire({
                title: 'Terbitkan artikel?',
                text: '',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#428B67',
                confirmButtonText: 'Terbitkan'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "{{ url('admin/post/update') }}",
                        method: 'POST',
                        data: data,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        contentType: false,
                        processData: false,
                        success: function(res) {
                            Swal.fire({
                                title: res.title,
                                text: res.text,
                                icon: res.icon,
                            }).then((result) => {
                                window.location.href = "{{ url('admin/post') }}";
                            });
                        }
                    });
                }
            });
        }
    });

    $('#draftButton').on('click', function(e) {
        if ($('#dataForm').parsley().isValid()) {
            e.preventDefault();
            var data = new FormData();
            var image = $('#cover')[0].files;

            if (image.length > 0) {
                data.append('cover', image[0]);
            } else {
                data.append('cover', 'not');
            }

            data.append('id', $('#id').val());
            data.append('title', $('#title').val());
            data.append('content', CKEDITOR.instances['content'].getData());
            data.append('category_id', $('#category_id option:selected').val());
            data.append('tags', $('#tags').val());
            data.append('type', 'DRAFT');

            Swal.fire({
                title: 'Arsipkan artikel?',
                text: 'Artikel dapat diterbitkan di lain waktu',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#428B67',
                confirmButtonText: 'Arsipkan'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "{{ url('admin/post/update') }}",
                        method: 'POST',
                        data: data,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        contentType: false,
                        processData: false,
                        success: function(res) {
                            Swal.fire({
                                title: res.title,
                                text: res.text,
                                icon: res.icon,
                            }).then((result) => {
                                window.location.href = "{{ url('admin/post') }}";
                            });
                        }
                    });
                }
            });
        }
    });
</script>
@endpush