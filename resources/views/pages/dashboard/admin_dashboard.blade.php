@extends('layouts.main')
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Dashboard</h4>
        <ol class="breadcrumb m-0">
            @if(Auth()->user()->role == 'admin')
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            @else
            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            @endif
        </ol>
    </div>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h5 class="header-title mb-4">Total artikel</h5>
                <div class="media">
                    <div class="media-body">
                        <h5 class="mb-0">{{ $total['post'] }}</h5>
                    </div>

                    <div class="align-self-end ml-2">
                        <a href="{{ url('admin/post') }}" class="btn btn-primary btn-sm">Detail <i class="mdi mdi-arrow-right ml-1"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h5 class="header-title mb-4">Total sharing</h5>
                <div class="media">
                    <div class="media-body">
                        <h5 class="mb-0">{{ $total['sharing'] }}</h5>
                    </div>

                    <div class="align-self-end ml-2">
                        <a href="{{ url('admin/sharing') }}" class="btn btn-primary btn-sm">Detail <i class="mdi mdi-arrow-right ml-1"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h5 class="header-title mb-4">Total karya</h5>
                <div class="media">
                    <div class="media-body">
                        <h5 class="mb-0">{{ $total['karya'] }}</h5>
                    </div>

                    <div class="align-self-end ml-2">
                        <a href="{{ url('admin/creation') }}" class="btn btn-primary btn-sm">Detail <i class="mdi mdi-arrow-right ml-1"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h5 class="header-title mb-4">Total pengguna</h5>
                <div class="media">
                    <div class="media-body">
                        <h5 class="mb-0">{{ $total['user'] }}</h5>
                    </div>

                    <div class="align-self-end ml-2">
                        <a href="{{ url('admin/user') }}" class="btn btn-primary btn-sm">Detail <i class="mdi mdi-arrow-right ml-1"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-9">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Statistik kunjungan</h4>
                <p class="card-title-desc">Statistik kunjungan pengguna ke web dan artikel</p>
                <div id="stats-reader" style="height: 320px"></div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mb-3">Sharing terbaru</h4>
                @forelse($latestTopic as $topic)
                <h5><a href="{{ url('admin/sharing/detail/' . $topic->id) }}">{{ Str::limit($topic->topic, 35) }}</a></h5>
                <p>Oleh {{ $topic->user->name }}</p>
                <hr>
                @empty
                @endforelse
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script src="{{ url('assets/libs/flot-charts/jquery.flot.js') }}"></script>
<script src="{{ url('assets/libs/flot-charts/jquery.flot.time.js') }}"></script>
<script src="{{ url('assets/libs/jquery.flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ url('assets/libs/flot-charts/jquery.flot.resize.js') }}"></script>
<script src="{{ url('assets/libs/flot-charts/jquery.flot.pie.js') }}"></script>
<script src="{{ url('assets/libs/flot-charts/jquery.flot.selection.js') }}"></script>
<script src="{{ url('assets/libs/flot-charts/jquery.flot.stack.js') }}"></script>
<script src="{{ url('assets/libs/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
<script src="{{ url('assets/libs/flot-charts/jquery.flot.crosshair.js') }}"></script>
<script>
    var data = [];

    @foreach($stats as $stat)
    data.push(["{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', \Carbon\Carbon::parse($stat->tanggal), 'Asia/Jakarta')->timestamp }}", +'{{ $stat->stats }}']);
    @endforeach

    data.reverse();

    $.plot($("#stats-reader"), [{
        data: data,
        label: "Total pengunjung",
        color: "#f06543"
    }], {
        series: {
            lines: {
                show: !0,
                lineWidth: 2
            },
            shadowSize: 0
        },
        points: {
            show: !0
        },
        legend: {
            position: "ne",
            margin: [0, -32],
            noColumns: 0,
            labelBoxBorderColor: null,
            labelFormatter: function(o, e) {
                return o + "&nbsp;&nbsp;"
            },
            width: 30,
            height: 2
        },
        tooltip: !0,
        tooltipOpts: {
            content: "%s : %y.0",
            shifts: {
                x: -30,
                y: -50
            }
        },
        grid: {
            hoverable: !0,
            clickable: !0,
            borderColor: "rgba(123, 145, 158,0.1)",
            borderWidth: 0,
            labelMargin: 5,
            backgroundColor: "transparent"
        },
        yaxis: {
            min: 0,
            tickColor: "rgba(123, 145, 158,0.1)",
            font: {
                color: "#7b919e",
                size: 10
            }
        },
        xaxis: {
            tickColor: "rgba(123, 145, 158,0.1)",
            font: {
                color: "#7b919e",
                size: 10
            },
            tickFormatter: function(val, axis) {
                day = new Date(val * 1000);
                return day.getDate() + '/' + (day.getMonth() + 1) + '/' + day.getFullYear();
            },
        }
    });
</script>
@endpush