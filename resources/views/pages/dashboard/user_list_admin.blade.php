@extends('layouts.main')
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Kelola User</h4>
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">User</li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table table-bordered dt-responsive nowrap" id="dataTable">
                        <thead>
                            <th style="width: 15px;">No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th style="width: 20px;"><i class="mdi mdi-apple-keyboard-command"></i></th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="dataModal" tabindex="-1" aria-labelledby="dataModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dataModalLabel">Detail User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <h3 id="det_user_name"></h3>
                        <p id="det_user_email"></p>
                        <hr>
                        <div class="row text-center">
                            <div class="col-6">
                                <p>Total karya</p>
                                <h4 id="det_karya_total"></h4>
                            </div>
                            <div class="col-6">
                                <p>Total topic</p>
                                <h4 id="det_topic_total"></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    var table = $('#dataTable').DataTable({
        paginate: true,
        info: true,
        sort: true,
        processing: true,
        serverSide: true,
        order: [1, 'ASC'],
        ajax: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('admin/user') }}",
        },
        columns: [{
                data: 'DT_RowIndex',
                orderable: false,
                searchable: false,
                class: 'text-center',
                width: '10px'

            },
            {
                data: 'name',
            },
            {
                data: 'email',
            },
            {
                data: 'action'
            }
        ]
    });

    $(document).on('click', '.detailButton', function() {
        get_user($(this).data('id'));
        $('#dataModal').modal('show');
    });

    function get_user(id) {
        $.ajax({
            url: "{{ url('admin/user/detail') }}",
            method: 'POST',
            data: {
                id: id
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(res) {
                $('#det_user_name').html(res.detail.name);
                $('#det_user_email').html(res.detail.email);
                $('#det_karya_total').html(res.totalCreation);
                $('#det_topic_total').html(res.totalTopic);
            }
        });
    }
</script>
@endpush