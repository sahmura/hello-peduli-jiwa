@extends('layouts.main')
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Dashboard</h4>
        <ol class="breadcrumb m-0">
            @if(Auth()->user()->role == 'admin')
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            @else
            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            @endif
        </ol>
    </div>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Selamat datang</h4>
                <p class="card-title-desc">Di Hello Peduli Jiwa!</p>
                <p class="lead">{{ Auth()->user()->name}}</p>
            </div>
        </div>
    </div>
    <!-- <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title">Sharing terbaru</h4>
            </div>
        </div>
    </div> -->
</div>
@endsection
@push('js')
<script src="{{ url('assets/libs/flot-charts/jquery.flot.js') }}"></script>
<script src="{{ url('assets/libs/flot-charts/jquery.flot.time.js') }}"></script>
<script src="{{ url('assets/libs/jquery.flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ url('assets/libs/flot-charts/jquery.flot.resize.js') }}"></script>
<script src="{{ url('assets/libs/flot-charts/jquery.flot.pie.js') }}"></script>
<script src="{{ url('assets/libs/flot-charts/jquery.flot.selection.js') }}"></script>
<script src="{{ url('assets/libs/flot-charts/jquery.flot.stack.js') }}"></script>
<script src="{{ url('assets/libs/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
<script src="{{ url('assets/libs/flot-charts/jquery.flot.crosshair.js') }}"></script>
<script>
    $.plot($("#stats-reader"), [{
        data: [
            [0, 10],
            [1, 7],
            [2, 8],
            [3, 9],
            [4, 6],
            [5, 5],
            [6, 7]
        ],
        label: "New Customer",
        color: "#f06543"
    }, {
        data: [
            [0, 8],
            [1, 5],
            [2, 6],
            [3, 8],
            [4, 4],
            [5, 3],
            [6, 6]
        ],
        label: "Returning Customer",
        color: "#3ddc97"
    }], {
        series: {
            lines: {
                show: !0,
                lineWidth: 2
            },
            shadowSize: 0
        },
        points: {
            show: !0
        },
        legend: {
            position: "ne",
            margin: [0, -32],
            noColumns: 0,
            labelBoxBorderColor: null,
            labelFormatter: function(o, e) {
                return o + "&nbsp;&nbsp;"
            },
            width: 30,
            height: 2
        },
        tooltip: !0,
        tooltipOpts: {
            content: "%s : %y.0",
            shifts: {
                x: -30,
                y: -50
            }
        },
        grid: {
            hoverable: !0,
            clickable: !0,
            borderColor: "rgba(123, 145, 158,0.1)",
            borderWidth: 0,
            labelMargin: 5,
            backgroundColor: "transparent"
        },
        yaxis: {
            min: 0,
            max: 15,
            tickColor: "rgba(123, 145, 158,0.1)",
            font: {
                color: "#7b919e",
                size: 10
            }
        },
        xaxis: {
            tickColor: "rgba(123, 145, 158,0.1)",
            font: {
                color: "#7b919e",
                size: 10
            }
        }
    });
</script>
@endpush