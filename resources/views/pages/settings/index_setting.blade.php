@extends('layouts.main')
@section('header')
<div class="row align-items-center">
    <div class="col-md-8">
        <h4 class="page-title mb-1">Settings</h4>
        <ol class="breadcrumb m-0">
            @if(Auth()->user()->role == 'admin')
            <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Dashboard</a></li>
            @else
            <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            @endif
            <li class="breadcrumb-item"><a href="{{ url('/settings') }}">Settings</a></li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h5 class="header-title mb-4">Pengaturan akun</h5>
                <form action="" id="userform" data-parsley-validate>
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ Auth()->user()->name }}" placeholder="Nama" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control" value="{{ Auth()->user()->email }}" placeholder="Email" required>
                    </div>
                    <div class="form-group text-right">
                        <button class="btn btn-primary waves-effect" id="saveUserData">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h5 class="header-title mb-4">Pengaturan password</h5>
                <form action="" id="passwordform" data-parsley-validate>
                    <div class="form-group">
                        <label for="oldPassword">Password lama</label>
                        <input type="password" name="oldPassword" id="oldPassword" placeholder="Password lama" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="newPassword">Password baru</label>
                        <input type="password" name="newPassword" id="newPassword" placeholder="Password baru" class="form-control" required data-parsley-minlength="8">
                    </div>
                    <div class="form-group">
                        <label for="confirmPassword">Konfirmasi password baru</label>
                        <input type="password" name="confirmPassword" id="confirmPassword" placeholder="Konfirmasi password baru" class="form-control" required data-parsley-minlength="8" data-parsley-equalto="#newPassword">
                        <small class="text-danger" style="display: none;" id="password-error">Error. Password baru tidak sesuai</small>
                    </div>
                    <div class="form-group text-right">
                        <button class="btn btn-primary waves-effect" id="savePasswordData" disabled>Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $('#passwordform').parsley();
    $('#userform').parsley();

    $('#saveUserData').on('click', function(e) {
        var data = $('#userform').serialize();
        if ($('#userform').parsley().isValid()) {
            e.preventDefault();
            $.ajax({
                url: "{{ url('settings/updateProfil') }}",
                method: 'POST',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(res) {
                    Swal.fire({
                        title: res.title,
                        text: res.text,
                        icon: res.icon,
                    }).then((result) => {
                        window.location.reload();
                    });
                }
            });
        }
    });

    $('#savePasswordData').on('click', function(e) {
        var data = $('#passwordform').serialize();
        if ($('#passwordform').parsley().isValid()) {
            e.preventDefault();
            Swal.fire({
                title: 'Apakah kamu yakin?',
                text: 'Password akan diubah',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#E7472C'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "{{ url('settings/updatePassword') }}",
                        method: 'POST',
                        data: data,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(res) {
                            Swal.fire({
                                title: res.title,
                                text: res.text,
                                icon: res.icon,
                            }).then((result) => {
                                window.location.reload();
                            });
                        }
                    });
                }
            });
        }
    });

    $('#confirmPassword').on('keyup', function() {
        if ($(this).val() != $('#newPassword').val()) {
            $("#password-error").show();
            $('#savePasswordData').prop('disabled', true);
        } else {
            $("#password-error").hide();
            $('#savePasswordData').prop('disabled', false);
        }
    });
</script>
@endpush