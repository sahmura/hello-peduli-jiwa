@extends('layouts.home')

@section('hero')
<section id="hero" class="d-flex align-items-center">
    <div class="container position-relative" data-aos="fade-up" data-aos-delay="100">
        <div class="row justify-content-center pt-5">
            <div class="col-xl-7 col-lg-9 text-center">
                <h1>Hello Peduli Jiwa</h1>
                <h2>Berbagi cerita, berbagi tawa</h2>
            </div>
        </div>
        <div class="text-center mt-5">
            <a href="{{ url('sharing') }}" class="btn btn-primary btn-lg px-5">Mulai Bercerita</a>
            <a href="{{ url('blog') }}" class="btn btn-outline-primary btn-lg px-5">Blog</a>
        </div>

        <div class="row icon-boxes">
            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="zoom-in" data-aos-delay="200">
                <div class="icon-box">
                    <div class="icon"><i class="ri-chat-3-line"></i></div>
                    <h4 class="title"><a href="">Sharing</a></h4>
                    <p class="description">Berbagi cerita bagi yang ingin bercerita</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="zoom-in" data-aos-delay="300">
                <div class="icon-box">
                    <div class="icon"><i class="ri-file-list-3-line"></i></div>
                    <h4 class="title"><a href="">Blog</a></h4>
                    <p class="description">Kumpulan artikel menarik untuk pembaca</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="zoom-in" data-aos-delay="400">
                <div class="icon-box">
                    <div class="icon"><i class="ri-mic-line"></i></div>
                    <h4 class="title"><a href="">Podcast</a></h4>
                    <p class="description">Berbagi canda dan cerita melalui podcast</p>
                </div>
            </div>

            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0" data-aos="zoom-in" data-aos-delay="500">
                <div class="icon-box">
                    <div class="icon"><i class="ri-layout-2-line"></i></div>
                    <h4 class="title"><a href="">Content</a></h4>
                    <p class="description">Konten menarik di media sosial Hello Peduli Jiwa</p>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection

@section('main')
<section id="about" class="about">
    <div class="container" data-aos="fade-up">

        <div class="section-title">
            <h2>Tentang Hello Peduli Jiwa</h2>
            <p>Bersama kita saling cerita</p>
        </div>

        <div class="row content">
            <div class="col-lg-6 text-justify">
                <p>Hello Peduli Jiwa merupakan suatu multiplatform yang menyediakan info, pengetahuan, dan wadah bagi mereka yang peduli pada diri sendiri. Melalui Hello Peduli Jiwa dengan berbagai platform yang ada diharapkan menjadi bagian dari masyarakat untuk membantu bagaimana kita menyayangi diri kita dan bagaimana menenangkan jiwa dari banyaknya beban dan pikiran.</p>
            </div>
            <div class="col-lg-6 pt-4 pt-lg-0 text-justify">
                <p>Sahabat Hello Peduli Jiwa dapat bercerita apa saja, membaca artikel menarik, mendengarkan podcast, dan melihat berbagai konten menarik lainnya. Dengan begitu, kami berharap teman-teman semua bisa lebih bahagia dan lebih semangat dalam menjalani hari-hari esok.</p>
                <p>- Agista Rahma Ditha</p>
            </div>
        </div>

    </div>
</section>

<!-- <section id="counts" class="counts section-bg">
    <div class="container">

        <div class="row justify-content-end">

            <div class="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
                <div class="count-box">
                    <span data-toggle="counter-up">65</span>
                    <p>Happy Clients</p>
                </div>
            </div>

            <div class="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
                <div class="count-box">
                    <span data-toggle="counter-up">85</span>
                    <p>Projects</p>
                </div>
            </div>

            <div class="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
                <div class="count-box">
                    <span data-toggle="counter-up">12</span>
                    <p>Years of experience</p>
                </div>
            </div>

            <div class="col-lg-3 col-md-5 col-6 d-md-flex align-items-md-stretch">
                <div class="count-box">
                    <span data-toggle="counter-up">15</span>
                    <p>Awards</p>
                </div>
            </div>

        </div>

    </div>
</section> -->

<section id="cta" class="cta">
    <div class="container" data-aos="zoom-in">
        <div class="text-center">
            <h3>Mulai bercerita</h3>
            <p>Kami menerima semua cerita dan curhatan teman-teman. Baik senang maupun sedih, baik ketika bersama teman maupun sedang sendiri. Kami ada untuk menemani.</p>
            <a class="cta-btn" href="{{ url('sharing') }}">Mulai bercerita</a>
        </div>
    </div>
</section>
@endsection